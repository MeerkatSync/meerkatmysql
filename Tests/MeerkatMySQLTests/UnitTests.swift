//
//  UnitTests.swift
//  
//
//  Created by Filip Klembara on 26/02/2020.
//

import XCTest
@testable import MeerkatMySQL
import MeerkatServerCore
import MeerkatSchemeDescriptor

private let refDate = Date()

final class UnitTests: XCTestCase {

    func testTransactionReduce() {
        let t1 = MySQLTransaction(deletions: [], creations: [], modifications: [
            MySQLSubTransaction(className: "Person", objectId: "o1", timestamp: refDate - 460, updates: [
                MySQLAttributeUpdate(attribute: "dogs", value: .arrayDiff(.init(original: ["a", "b", "c"], new: ["b", "c"])))
            ])
        ])
        let t2 = MySQLTransaction(deletions: [], creations: [], modifications: [
            MySQLSubTransaction(className: "Person", objectId: "o1", timestamp: refDate - 431, updates: [
                MySQLAttributeUpdate(attribute: "dogs", value: .arrayDiff(.init(original: ["b", "c"], new: ["b", "d", "e", "c", "f"])))
            ])
        ])
        let t3 = MySQLTransaction(deletions: [], creations: [], modifications: [
            MySQLSubTransaction(className: "Person", objectId: "o1", timestamp: refDate - 401, updates: [
                MySQLAttributeUpdate(attribute: "dogs", value: .arrayDiff(.init(original: ["b", "d", "e", "c", "f"], new: ["b", "d", "e", "f"])))
            ])
        ])
        let ts = [t1, t2, t3]
        let addedTs = ts.reduce(MySQLTransaction(deletions: [], creations: [], modifications: []) as Transaction) { $0.adding($1) }
        guard let res = addedTs as? MySQLTransaction else {
            XCTFail()
            return
        }
        XCTAssertEqual(res.creations.count, 0)
        XCTAssertEqual(res.deletions.count, 0)
        XCTAssertEqual(res.modifications.count, 1)

        guard let sub = res.modifications.first as? MySQLSubTransaction else {
            XCTFail()
            return
        }
        XCTAssertEqual(sub.timestamp, refDate - 401)
        XCTAssertEqual(sub.updates.count, 1)

        guard let update = sub.updates.first as? MySQLAttributeUpdate else {
            XCTFail()
            return
        }
        XCTAssertEqual(update.attribute, "dogs")
        XCTAssertTrue(update.value.isArray)
        guard let diff = update.value.asArrayDiff else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.original, ["a", "b", "c"])
        XCTAssertEqual(diff.original.applying(diff.difference)!, ["b", "d", "e", "f"])
    }

    func testTransactionReduce2() {
        let t1 = MySQLTransaction(deletions: [], creations: [], modifications: [
            MySQLSubTransaction(className: "Person", objectId: "o1", timestamp: refDate - 460, updates: [
                MySQLAttributeUpdate(attribute: "dogs", value: .arrayDiff(.init(original: ["1", "2", "3"], new: ["4", "1", "3"])))
            ])
        ])
        let t2 = MySQLTransaction(deletions: [], creations: [], modifications: [
            MySQLSubTransaction(className: "Person", objectId: "o1", timestamp: refDate - 431, updates: [
                MySQLAttributeUpdate(attribute: "dogs", value: .arrayDiff(.init(original: ["1", "2", "3"], new: ["5", "3", "6"])))
            ])
        ])
        let ts = [t1, t2]
        let addedTs = ts.reduce(MySQLTransaction(deletions: [], creations: [], modifications: []) as Transaction) { $0.adding($1) }
        guard let res = addedTs as? MySQLTransaction else {
            XCTFail()
            return
        }
        XCTAssertEqual(res.creations.count, 0)
        XCTAssertEqual(res.deletions.count, 0)
        XCTAssertEqual(res.modifications.count, 1)

        guard let sub = res.modifications.first as? MySQLSubTransaction else {
            XCTFail()
            return
        }
        XCTAssertEqual(sub.timestamp, refDate - 431)
        XCTAssertEqual(sub.updates.count, 1)

        guard let update = sub.updates.first as? MySQLAttributeUpdate else {
            XCTFail()
            return
        }
        XCTAssertEqual(update.attribute, "dogs")
        XCTAssertTrue(update.value.isArray)
        guard let diff = update.value.asArrayDiff else {
            XCTFail()
            return
        }
        XCTAssertEqual(diff.original, ["1", "2", "3"])
        XCTAssertEqual(diff.original.applying(diff.difference)!, ["4", "5", "3", "6"])
    }

    static var allTests = [
        ("testTransactionReduce", testTransactionReduce),
        ("testTransactionReduce2", testTransactionReduce2),
    ]
}
