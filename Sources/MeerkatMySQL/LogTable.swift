//
//  LogTable.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 31/01/2020.
//

struct LogTable {
    var id: Int?

    let timestamp: Date
    let updateType: UpdateType

    let objectId: ObjectTable.ID
    let signatureId: SignatureTable.ID

    static var name = "sync_log"
    static var entity = "sync_logs"
}

extension LogTable {
    var object: Parent<LogTable, ObjectTable> {
        return parent(\.objectId)
    }

    var signature: Parent<LogTable, SignatureTable> {
        return parent(\.signatureId)
    }

    var attributes: Children<LogTable, AttributeTable> {
        return children(\.logId)
    }
}

extension LogTable: MySQLModel { }
extension LogTable: MySQLMigration { }

enum UpdateType: String, MySQLEnumType {
    static func reflectDecoded() throws -> (UpdateType, UpdateType) {
        return (.creation, .deletion)
    }
    case creation
    case deletion
    case modification
}
