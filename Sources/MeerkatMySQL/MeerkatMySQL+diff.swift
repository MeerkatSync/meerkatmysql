//
//  MeerkatMySQL+diff.swift
//  
//
//  Created by Filip Klembara on 26/02/2020.
//

extension MeerkatMySQL {
    func getGroupsAndRoles(userId: UserID, groups: [Group]) -> Future<[(GroupTable, RoleTable)]> {
        withDB { conn -> EventLoopFuture<[(GroupTable, RoleTable)]> in
            let getUser = MeerkatMySQL.getUserFuture(by: userId, on: conn)
            let getGroupsAndRoles = getUser.flatMap { user in GroupTable.query(on: conn).filter(\.name ~~ groups.map { $0.id }).join(\RoleTable.group, to: \GroupTable.id).alsoDecode(RoleTable.self).filter(\RoleTable.user == user.id!).all() }
            return getGroupsAndRoles
        }
    }

    typealias GroupRoleSignatures = (GroupTable, Role, [SignatureTable])

    func getGroupRoleSignature(userId: UserID, groups: [Group]) -> Future<[GroupRoleSignatures]> {
        let versions = Dictionary(groups.map { ($0.id, $0.version) }) { min($0, $1) }
        let getAllGroupsRoleSignatures = getGroupsAndRoles(userId: userId, groups: groups).innerFlatMapFlatten { groupRole -> EventLoopFuture<GroupRoleSignatures> in
            let (group, role) = groupRole
            let getSignatures = self.withDB { conn in try group.signatures.query(on: conn).filter(\.version > versions[group.name] ?? 0).sort(\.version).all() }
            let b = getSignatures.map { signatures -> GroupRoleSignatures in
                guard !signatures.isEmpty else {
                    return (group, role.role.asRole, [])
                }
                return (group, role.role.asRole, signatures)
            }
            return b
        }
        return getAllGroupsRoleSignatures
    }

    func getGroupWithVersion(group: GroupTable, role: Role, transactions: [Transaction]) -> Future<DiffGroupUpdate> {
        let getVersion = self.withDB { conn in try group.signatures.query(on: conn).max(\.version).mapIfError { _ in 0 } }
        let objs = transactions.flatMap { $0.creations }.map { MySQLObjectDescription(createSubtransaction: $0) }
        return getVersion.map { MySQLDiffGroupUpdate(group: MySQLGroup(id: group.name, version: $0), newObjects: objs, role: role) }
    }

    struct MyRow: Reflectable, Decodable {
        let timestamp: Date
        let updateType: UpdateType
        let className: String
        let objectId: ObjectID
        let key: String?
        let value: Data?
    }

    func reduceSubtransactions(_ subs: [MyRow], in groupId: GroupID) throws -> Transaction {
        var deletions = [SubTransaction]()
        var creations = [CreationDescription]()
        var modifications = [SubTransaction]()
        try subs.reversed().forEach { sub in
            switch sub.updateType {
            case .creation:
                let ups: [Update]
                if let key = sub.key {
                    let up = try MySQLAttributeUpdate(attribute: key, value: self.scheme.getValue(for: key, from: sub.value, in: sub.className, prefferedArrayRepresentation: .arrayDiff))
                    ups = [up]
                } else {
                    ups = []
                }
                let s = MySQLSubTransaction(className: sub.className, objectId: sub.objectId, timestamp: sub.timestamp, updates: ups)
                creations.append(MySQLCreationDescription(groupID: groupId, object: s))
            case .deletion:
                let s = MySQLSubTransaction(className: sub.className, objectId: sub.objectId, timestamp: sub.timestamp, updates: [])
                deletions.append(s)
            case .modification:
                guard let key = sub.key else {
                    return
                }
                let up = try MySQLAttributeUpdate(attribute: key, value: self.scheme.getValue(for: key, from: sub.value, in: sub.className, prefferedArrayRepresentation: .arrayDiff))
                let s = MySQLSubTransaction(className: sub.className, objectId: sub.objectId, timestamp: sub.timestamp, updates: [up])
                modifications.append(s)
            }
        }
        return MySQLTransaction(deletions: deletions, creations: creations, modifications: modifications).reduce()
    }


    private func getTransaction(for group: GroupTable, from signature: GroupVersion) -> Future<Transaction> {
        let rows = withDB { conn -> Future<[MyRow]> in
            conn.select().column(\LogTable.updateType).column(\LogTable.timestamp).column(\ObjectTable.className).column(\ObjectTable.objectId).column(\AttributeTable.key).column(\AttributeTable.value)
                .from(GroupTable.self).where(\GroupTable.id == group.id).join(\GroupTable.id, to: \SignatureTable.groupId).where(\SignatureTable.version, .greaterThan, signature).join(\SignatureTable.id, to: \LogTable.signatureId).join(\LogTable.objectId, to: \ObjectTable.id).join(\LogTable.id, to: \AttributeTable.logId, method: .left).all(decoding: MyRow.self)
        }
        return rows.map { try self.reduceSubtransactions($0, in: group.name) }

    }

    public func diffs(for user: User, in groups: [Group]) -> Future<Diff> {
        let versions = Dictionary(groups.map { ($0.id, $0.version) }) { min($0, $1) }
        let getSplittedGroupsRoleSignatures = getGroupRoleSignature(userId: user.id, groups: groups)
            .map { arr -> ([(GroupTable, Role)], [(GroupTable, Role)]) in

                var valid = [(GroupTable, Role)]()
                var outdated = [(GroupTable, Role)]()
                for (g, r, ss) in arr {
                    if ss.isEmpty {
                        valid.append((g, r))
                    } else {
                        outdated.append((g, r))
                    }
                }
                return (valid, outdated)
        }

        let getValidGroups = getSplittedGroupsRoleSignatures.map { (valid, _) in valid }
        let getOutdatedGroups = getSplittedGroupsRoleSignatures.map { (_, outdated) in outdated }
        let getValidGroupsDesc = getValidGroups.innerFlatMapFlatten { self.getGroupWithVersion(group: $0, role: $1, transactions: []) }

        let getGroupRoleOutdatedTransactionsByGroup = getOutdatedGroups.innerFlatMapFlatten { groupRole -> EventLoopFuture<(GroupTable, Role, [Transaction])> in
            let (group, role) = groupRole
            let trans = self.getTransaction(for: group, from: versions[group.name]!)
            return trans.map { (group, role, [$0]) }
        }
        let getOutdatedGroupsDesc = getGroupRoleOutdatedTransactionsByGroup.innerFlatMapFlatten(closure: getGroupWithVersion)

        let getTrans = getGroupRoleOutdatedTransactionsByGroup.map { $0.flatMap { $0.2 } }
        let getGroupUpdates = getOutdatedGroupsDesc.and(getValidGroupsDesc).map { $0 + $1 }
        let getDiff: EventLoopFuture<Diff> = getGroupUpdates.and(getTrans).map { MySQLDiff(groups: $0, transactions: $1) }

        return getDiff
    }

}
