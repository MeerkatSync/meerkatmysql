//
//  MeerkatMySQL.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 31/01/2020.
//

public final class MeerkatMySQL {
    var mysql: Container
    let scheme: MeerkatSchemeDescriptor
    let reduceAttributesHistory: Bool
    
    private init(scheme: MeerkatSchemeDescriptor, container: Container, reduceAttributesHistory: Bool = true) {
        self.scheme = scheme
        mysql = container
        self.reduceAttributesHistory = reduceAttributesHistory
    }
}

extension MeerkatMySQL: ServiceType {
    public static func makeService(for container: Container) throws -> Self {
        let scheme = try container.make(MeerkatSchemeDescriptor.self)
        return .init(scheme: scheme, container: container)
    }

    public static var serviceSupports: [Any.Type] {
        return [DBWrapper.self, NotificatorDBWrapper.self]
    }
}

extension MeerkatMySQL: DBWrapper {
    public func remove(user: User) -> EventLoopFuture<Void> {
        let query = withDB { conn -> Future<Void> in
            let getUser = MeerkatMySQL.getUserFuture(by: user.id, on: conn)
            let userExists = { getUser.transform(to: true).thenIfErrorThrowing { error in
                guard case .unknownUser = error as? DBWrapperError else {
                    throw error
                }
                return false
                }}
            let usersDevices = { getUser.flatMap { try $0.devices.query(on: conn).all() } }

            let usersGroups = getUser.flatMap { try $0.groups.query(on: conn).all() }
            let removeRoles = { self.withDB { conn in getUser.flatMap { u -> Future<Void> in try u.groups.pivots(on: conn).delete() } } }
            let groupsOwnedOnlyByUser = {
                usersGroups.innerFlatMapFlatten { g -> Future<GroupTable?> in
                    try g.users
                        .query(on: conn)
                        .filter(\.username != user.id)
                        .filter(\RoleTable.role == .owner)
                        .first()
                        .map { $0 == nil ? g : nil }
                }.map { $0.compactMap { $0 } }
            }
            let removeOwnedGroups = {
                groupsOwnedOnlyByUser().innerFlatMapFlatten {
                    MeerkatMySQL.delete(group: $0, on: conn)
                }.transform(to: ())
            }
            let removeDevices = {
                usersDevices().innerFlatMapFlatten { $0.delete(on: conn) }
            }
            let removeUser = { getUser.delete(on: conn) }
            return userExists().flatMap { exists in
                guard exists else {
                    return conn.future()
                }
                return removeDevices().and(removeRoles()).flatMap { _ in removeOwnedGroups() }.flatMap { _ in removeUser() }.transform(to: ())
            }
        }
        return query
    }

    public func addUser(withId id: UserID) -> Future<User> {
        let query = withDB { conn -> Future<User> in
            let userExists = MeerkatMySQL.getUserFuture(by: id, on: conn).transform(to: true).mapIfError { _ in false }
            return userExists.flatMap { exists in
                guard !exists else {
                    throw DBWrapperError.userAlreadyExists(id: id)
                }
                return UserTable(username: id).save(on: conn).map { MySQLUser(id: $0.username) as User }
            }
        }
        return query
    }

    public func users(for group: Group) -> Future<[UserRole]> {
        let query = withDB { conn -> Future<[UserRole]> in
            let gg = MeerkatMySQL.getGroupFuture(by: group.id, on: conn)
            let usersRoleRows = gg.flatMap { g in
                try g.users.query(on: conn).alsoDecode(RoleTable.self).all()
            }
            let users = usersRoleRows.map { $0.map { UserRole(user: MySQLUser(id: $0.username), role: $1.role.asRole) } }
            return users
        }
        return query
    }

    public func user(by userId: UserID) -> Future<User> {
        return mysql.withPooledConnection(to: .mysql) { UserTable.query(on: $0).filter(\.username == userId).first() }
            .map { $0.flatMap { MySQLUser(id: $0.username) } }.unwrap(or: DBWrapperError.unknownUser(id: userId))
    }

    public func groups(for user: User) -> Future<[Group]> {
        let query = mysql.withNewConnection(to: .mysql) { conn -> EventLoopFuture<[MySQLGroup]> in
            conn.select()
                .groupBy(\GroupTable.id)
                .column(.column(\GroupTable.name), as: "id")
                .column(.function("MAX", [.expression(.column(\SignatureTable.version))]), as: "version")
                .from(RoleTable.self)
                .join(\RoleTable.user, to: \UserTable.id)
                .where(\UserTable.username == user.id)
                .join(\RoleTable.group, to: \GroupTable.id)
                .join(\GroupTable.id, to: \SignatureTable.groupId, method: .left)
                .all(decoding: MySQLGroup.self)
        }.map { $0 as [Group] }
        return query.thenIfErrorThrowing { throw DBWrapperError.databaseError($0) }
    }

    public func group(by groupId: GroupID) -> Future<Group> {
        let query = mysql.withPooledConnection(to: .mysql) { conn -> EventLoopFuture<MySQLGroup> in
            MeerkatMySQL.getMySQLGroupFeature(by: groupId, on: conn)
        }.map { $0 as Group }
        return query
    }

    public func addGroup(withId groupId: GroupID, owner: User, objects: [ObjectDescription]) -> Future<Group> {
        let groupCheck = group(by: groupId).transform(to: false).mapIfError { _ in true }
        let objects = objects.map { ($0.className, $0.objectId) }.reduce(into: [String: [ObjectID]]()) { (objs, o) in
            if let arr = objs[o.0] {
                objs[o.0] = arr + [o.1]
            } else {
                objs[o.0] = [o.1]
            }
        }

        return groupCheck.flatMap { ok in
            guard ok else {
                throw DBWrapperError.groupAlreadyExists(id: groupId)
            }
            return self.mysql.withPooledConnection(to: .mysql) { conn -> EventLoopFuture<GroupTable> in
                conn.transaction(on: .mysql) { con in
                    GroupTable(name: groupId).save(on: con).then { group in
                        let objmap = objects.map { cls, objs in
                            ObjectTable.query(on: con).filter(\.className == cls).filter(.make(\.objectId, .in, objs)).all()
                        }
                        let objs = objmap.map { f -> EventLoopFuture<[ObjectTable]> in
                            f.then { arr in
                                arr.map { (o: ObjectTable) in
                                    MeerkatMySQL.move(o, to: group, on: con)
                                }.flatten(on: con)
                            }
                        }
                        let userFuture = self.withDB { conn in UserTable.query(on: con).filter(\.username == owner.id)
                            .first()
                            .unwrap(or: DBWrapperError.unknownUser(id: owner.id))
                            .then { RoleTable(user: $0.id!, group: group.id!, role: .owner).save(on: con) } }

                        return objs.flatten(on: con).and(userFuture).map { _ in group }
                    }
                }
            }.map { MySQLGroup(id: $0.name, version: 0) as Group }
        }
    }

    public func subscribe(_ user: User, to group: Group, by owner: User, as newRole: Role) -> Future<Void> {
        let query = mysql.withPooledConnection(to: .mysql) { conn -> EventLoopFuture<Void> in
            let getUser = self.withDB { conn in MeerkatMySQL.getUserFuture(by: user.id, on: conn) }
            let getOwner = self.withDB { conn in MeerkatMySQL.getUserFuture(by: owner.id, on: conn) }
            let getGroup = self.withDB { conn in GroupTable.query(on: conn).filter(\.name == group.id).first().unwrap(or: DBWrapperError.unknownGroup(id: group.id)) }
            let getRole = getGroup.and(getOwner).thenThrowing { g, u in
                self.withDB { conn in try u.groups.pivots(on: conn).filter(\.group == g.id!).filter(\.user == u.id!).filter(\.role == .owner).first() }
            }.flatMap { $0 }.unwrap(or: DBWrapperError.wrongPermissions(for: user, is: .read, minimum: .owner))
            let getUserRole = getGroup.and(getUser).thenThrowing { g, u in
                self.withDB { conn in try u.groups.pivots(on: conn).filter(\.group == g.id!).filter(\.user == u.id!).first() }
            }.flatMap { $0 }
            let setRole = getRole.and(getUserRole).then { _, role -> EventLoopFuture<RoleTable> in
                if let r = role {
                    return RoleTable(id: r.id, user: r.user, group: r.group, role: .init(role: newRole)).update(on: conn)
                } else {
                    return getGroup.and(getUser).flatMap { RoleTable(user: $1.id!, group: $0.id!, role: .init(role: newRole)).create(on: conn) }
                }
            }
            return setRole.transform(to: ())
        }
        return query
    }

    public func unsubscribe(_ user: User, from group: Group, by remover: User) -> Future<Void> {
        let query = mysql.withPooledConnection(to: .mysql) { con -> EventLoopFuture<Void> in
            con.transaction(on: .mysql) { conn -> EventLoopFuture<Void> in
                let getUser = self.withDB { conn in MeerkatMySQL.getUserFuture(by: user.id, on: conn) }
                let getRemover = self.withDB { conn in MeerkatMySQL.getUserFuture(by: remover.id, on: conn) }
                let getGroup = self.withDB { conn in MeerkatMySQL.getGroupFuture(by: group.id, on: conn) }
                let getCanRemove = getGroup.and(getRemover).thenThrowing { gr, u -> EventLoopFuture<RoleTable> in
                    try gr.users.pivots(on: conn).filter(\.user == u.id!).first().unwrap(or: DBWrapperError.userNotInGroup(user: user, group: group))
                }.flatMap { $0 }.thenThrowing { role -> Bool in
                    if role.role == .owner || remover.id == user.id {
                        return true
                    } else {
                        throw DBWrapperError.wrongPermissions(for: remover, is: role.role.asRole, minimum: .owner)
                    }
                }
                let roleDeletion = getCanRemove.then { _ in
                    getGroup.and(getUser).flatMap { gr, u -> EventLoopFuture<RoleTable?> in
                        try gr.users.pivots(on: conn).filter(\.user == u.id!).first()
                    }.flatMap { optR -> Future<Void> in
                        guard let r = optR else { return conn.future() }
                        return r.delete(on: conn)
                    }
                }

                let getCount = roleDeletion.transform(to: getGroup).flatMap { group in
                    try group.users.query(on: conn).count()
                }
                return getCount.then { c in
                    guard c == 0 else { return conn.future() }
                    return  getGroup.flatMap { MeerkatMySQL.delete(group: $0, on: conn) }
                }
            }
        }
        return query
    }

    public func remove(group: Group, by user: User) -> Future<Void> {
        let query = mysql.withPooledConnection(to: .mysql) { conn -> EventLoopFuture<Void> in
            let getGroup = self.withDB { conn in MeerkatMySQL.getGroupFuture(by: group.id, on: conn) }
            let getUser = self.withDB { conn in MeerkatMySQL.getUserFuture(by: user.id, on: conn) }
            let canRemove = getGroup.and(getUser).thenThrowing { group, u in
                try group.users.pivots(on: conn).filter(\.user == u.id!).filter(\.role == .owner).first().unwrap(or: DBWrapperError.wrongPermissions(for: user, is: .read, minimum: .owner))
            }.flatMap { $0 }
            return canRemove.then { _ in getGroup }.flatMap { MeerkatMySQL.delete(group: $0, on: conn) }
        }
        return query
    }

    public func role(of user: User, in group: Group) -> Future<Role> {
        let query = withDB { conn -> Future<Role> in
            MeerkatMySQL.getUserFuture(by: user.id, on: conn).flatMap { u -> Future<RoleTable> in
                try u.groups.query(on: conn).filter(\.name == group.id).decode(RoleTable.self).first().unwrap(or: DBWrapperError.userNotInGroup(user: user, group: group))
            }.map { $0.role.asRole }
        }
        return query
    }
}

extension MeerkatMySQL: NotificatorDBWrapper {

    public func add(device: Device, for user: User) -> EventLoopFuture<Void> {
        let query = withDB { conn in
            MeerkatMySQL.getUserFuture(by: user.id, on: conn).flatMap {
                DeviceTable(device: device.id, info: device.description, userId: $0.id!).create(on: conn)
            }
        }.transform(to: ())
        return query
    }

    public func remove(deviceId: String, for user: User) -> Future<Void> {
        let query = withDB { conn in
            MeerkatMySQL.getUserFuture(by: user.id, on: conn).flatMap {
                try $0.devices.query(on: conn).filter(\.device == deviceId).delete()
            }
        }
        return query
    }

    public func devices(for user: User) -> Future<[Device]> {
        let query = withDB { conn in
            MeerkatMySQL.getUserFuture(by: user.id, on: conn).flatMap {
                try $0.devices.query(on: conn).all().map { $0.map { MySQLDevice(id: $0.device, description: $0.info) as Device } }
            }
        }
        return query
    }
}

extension MeerkatMySQL {
    static func delete(group: GroupTable, on connection: MySQLConnection) -> EventLoopFuture<Void> {
        MeerkatMySQL.deleteSignatures(in: group, on: connection).flatMap { ok -> EventLoopFuture<Void> in
            connection.future().flatMap { try group.users.pivots(on: connection).delete() }
        }.transform(to: deleteObjects(in: group, on: connection)).transform(to: group.delete(on: connection))
    }

    static func deleteSignatures(in group: GroupTable, on connection: MySQLConnection) -> EventLoopFuture<Void> {
        let signatures = connection.future().flatMap { try group.signatures.query(on: connection).all() }
        let q = signatures.innerFlatMapFlatten { MeerkatMySQL.deleteLogs(for: $0, on: connection) }.transform(to: ())
        let query = q.flatMap { _ in signatures.innerFlatMapFlatten { $0.delete(on: connection) } }
        return query.transform(to: ())

    }

    static func deleteLogs(for signature: SignatureTable, on connection: MySQLConnection) -> Future<Void> {
        let logs = connection.future().flatMap { _ in
            try signature.logs.query(on: connection).all()
        }
        return logs.innerFlatMapFlatten { l in deleteAttributes(for: l, on: connection).flatMap { _ in l.delete(on: connection) } }.transform(to: ())
    }

    static func deleteAttributes(for log: LogTable, on connection: MySQLConnection) -> Future<Void> {
        let attributes = connection.future().flatMap { try log.attributes.query(on: connection).all() }
        return attributes.innerFlatMapFlatten { $0.delete(on: connection) }.transform(to: ())
    }

    static func deleteObjects(in group: GroupTable, on connection: MySQLConnection) -> EventLoopFuture<Void> {
        connection.future().flatMap{ try group.objects.query(on: connection).delete() }
    }

    static func getUserFuture(by id: UserID, on connection: MySQLConnection) -> EventLoopFuture<UserTable> {
        return UserTable.query(on: connection).filter(\.username == id).first().unwrap(or: DBWrapperError.unknownUser(id: id))
    }

    static func getGroupFuture(by id: GroupID, on connection: MySQLConnection) -> EventLoopFuture<GroupTable> {
        return GroupTable.query(on: connection).filter(\.name == id).first().unwrap(or: DBWrapperError.unknownGroup(id: id))
    }

    private static func move(_ object: ObjectTable, to group: GroupTable, on connection: MySQLConnection) -> EventLoopFuture<ObjectTable> {
        return ObjectTable(id: object.id, className: object.className, objectId: object.objectId, groupId: group.id!)
            .update(on: connection)
    }

    func mergeSubtransactions(groupId: GroupID, subtrans: [(UpdateType, SubTransaction)]) -> Transaction {
        var deletions = [SubTransaction]()
        var creations = [CreationDescription]()
        var modifications = [SubTransaction]()
        for (ut, sub) in subtrans {
            switch ut {
            case .creation:
                creations.append(MySQLCreationDescription(groupID: groupId, object: sub))
            case .deletion:
                deletions.append(sub)
            case .modification:
                modifications.append(sub)
            }
        }
        return MySQLTransaction(deletions: deletions, creations: creations, modifications: modifications)
    }

    func getSubtransaction(for log: LogTable, on connection: MySQLConnection) -> EventLoopFuture<(UpdateType, SubTransaction)> {
        let getAllAttributes = self.withDB { conn in try log.attributes.query(on: conn).all() }
        let getObject = log.object.get(on: connection)
        let getSubTransaction = getObject.and(getAllAttributes).map { object, attributes -> SubTransaction in
            let updates = try attributes.map { MySQLAttributeUpdate(attribute: $0.key, value: try self.scheme.getValue(for: $0.key, from: $0.value, in: object.className, prefferedArrayRepresentation: .arrayDiff)) }
            return MySQLSubTransaction(className: object.className, objectId: object.objectId, timestamp: log.timestamp, updates: updates)
        }
        return getSubTransaction.map { (log.updateType, $0) }
    }

    func withDB<T>(_ closure: @escaping (MySQLConnection) throws -> EventLoopFuture<T>) -> EventLoopFuture<T> {
        let query = mysql.withPooledConnection(to: .mysql) { conn -> Future<T> in
            conn.newBackgroundTransaction(closure)
        }
        return mysql.future().transform(to: query)
    }

    static func getMySQLGroupFeature(by id: GroupID, on conn: MySQLConnection) -> EventLoopFuture<MySQLGroup> {
        return conn.select()
        .groupBy(\GroupTable.id)
        .column(.column(\GroupTable.name), as: "id")
        .column(.function("MAX", [.expression(.column(\SignatureTable.version))]), as: "version")
        .from(GroupTable.self)
        .where(\GroupTable.name == id)
        .join(\GroupTable.id, to: \SignatureTable.groupId, method: .left)
            .first(decoding: MySQLGroup.self).unwrap(or: DBWrapperError.unknownGroup(id: id))
    }
}
extension MySQLConnection {

    func newBackgroundTransaction<T>(_ callback: @escaping (_ connection: MySQLConnection) throws -> Future<T>) -> EventLoopFuture<T>{
        //return self.transaction(on: .mysql, { (connection) throws  -> EventLoopFuture<T> in
        let connection = self
            let promise = connection.eventLoop.newPromise(T.self)
            DispatchQueue.global().async { () -> Void in
                do {
                    let a = try callback(connection)

                    promise.succeed(result: try a.wait())
                } catch (let error) {
                    promise.fail(error: error)
                }
            }
            return promise.futureResult
        //})
    }
}
