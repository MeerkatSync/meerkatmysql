//
//  AttributeTable.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 31/01/2020.
//

struct AttributeTable {
    var id: Int?

    let key: String
    let value: Data?

    let logId: LogTable.ID

    static var name = "sync_attribute"
    static var entity = "sync_attributes"
}

extension AttributeTable {
    var log: Parent<AttributeTable, LogTable> {
        return parent(\.logId)
    }
}

extension AttributeTable: MySQLModel { }
extension AttributeTable: MySQLMigration { }
