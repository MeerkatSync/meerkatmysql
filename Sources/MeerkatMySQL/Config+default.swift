//
//  Config+default.swift
//  Async
//
//  Created by Filip Klembara on 20/01/2020.
//

public extension Config {
    static let meerkatMySQLDefault: Config = {
        var conf = Config()
        conf.prefer(ConsoleLogger.self, for: Logger.self)
        return conf
    }()
}
