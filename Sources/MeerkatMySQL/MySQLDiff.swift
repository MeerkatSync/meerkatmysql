//
//  MySQLDiff.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 31/01/2020.
//

struct MySQLDiff: Diff {
    let groups: [DiffGroupUpdate]

    let transactions: [Transaction]
}

extension MySQLDiff: Equatable {
    static func == (lhs: MySQLDiff, rhs: MySQLDiff) -> Bool {
        let g1 = lhs.groups as! [MySQLDiffGroupUpdate]
        let g2 = rhs.groups as! [MySQLDiffGroupUpdate]
        let t1 = lhs.transactions as! [MySQLTransaction]
        let t2 = rhs.transactions as! [MySQLTransaction]
        return Set(g1) == Set(g2) && Set(t1) == Set(t2)
    }
}

struct MySQLTransaction: Transaction {
    let deletions: [SubTransaction]

    let creations: [CreationDescription]

    let modifications: [SubTransaction]

    init(deletions: [SubTransaction] = [], creations: [CreationDescription] = [], modifications: [SubTransaction] = []) {
        self.deletions = deletions
        self.creations = creations
        self.modifications = modifications
    }
}

extension MySQLTransaction: Hashable {
    static func == (lhs: MySQLTransaction, rhs: MySQLTransaction) -> Bool {
        let d1 = lhs.deletions as! [MySQLSubTransaction]
        let d2 = rhs.deletions as! [MySQLSubTransaction]
        let c1 = lhs.creations as! [MySQLCreationDescription]
        let c2 = rhs.creations as! [MySQLCreationDescription]
        let m1 = lhs.modifications as! [MySQLSubTransaction]
        let m2 = rhs.modifications as! [MySQLSubTransaction]
        let ok1 = Set(d1) == Set(d2)
        let ok2 = Set(c1) == Set(c2)
        let ok3 = Set(m1) == Set(m2)
        let ok = ok1 && ok2 && ok3
        guard ok else {
            return ok
        }
        return ok
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(deletions.count)
        hasher.combine(creations.count)
        hasher.combine(modifications.count)
    }
}

extension Transaction {
    private func reduceCreations() -> [MySQLObjectDescription: CreationDescription] {
        let cdt = creations.map { (MySQLObjectDescription(createSubtransaction: $0), $0) }
        let cd = Dictionary(cdt) { c1, c2 -> CreationDescription in
            let u1 = c1.object.updates
            let u1Ats = Set(u1.map { $0.attribute })
            let u2 = c2.object.updates
            let u2Ats = Set(u2.map { $0.attribute })
            var starting = c1.object.timestamp > c2.object.timestamp ? u1 : u2
            let additional = c1.object.timestamp > c2.object.timestamp ? u2 : u1
            let closed = c1.object.timestamp > c2.object.timestamp ? u1Ats : u2Ats
            additional.forEach { a in
                guard !closed.contains(a.attribute) else { return }
                starting.append(a)
            }
            return MySQLCreationDescription(groupID: c1.groupID,
                                            object: MySQLSubTransaction(className: c1.object.className,
                                                                        objectId: c1.object.objectId,
                                                                        timestamp: max(c1.object.timestamp, c2.object.timestamp),
                                                                        updates: starting))
        }
        return cd
    }

    private func reduceDeletions() -> [MySQLObjectDescription: SubTransaction] {
        let ddt = deletions.map { (MySQLObjectDescription(subtransaction: $0), $0) }
        let cd = Dictionary(ddt) { d1, d2 -> SubTransaction in d1.timestamp > d2.timestamp ? d1 : d2}
        return cd
    }

    private func reduceModifications() -> [MySQLObjectDescription: SubTransaction] {
        let ddt = modifications.map { (MySQLObjectDescription(subtransaction: $0), $0) }.sorted { a, b in a.1.timestamp < b.1.timestamp }
        let cd = Dictionary(ddt) { d1, d2 -> SubTransaction in
            let u1d = Dictionary(d1.updates.map { (MySQLHashableAttributeUpdate(update: $0), $0) }) { a, b in a }
            let u2d = Dictionary(d2.updates.map { (MySQLHashableAttributeUpdate(update: $0), $0) }) { a, b in a }
            let updates = u1d.merging(u2d) { a, b -> Update in
                if a.value.isArray && b.value.isArray {
                    switch (a.value, b.value) {
                    case (.array, _), (_, .array):
                        return d1.timestamp > d2.timestamp ? a : b
                    default:
                        let aDiff = a.value.asArrayDiff!
                        let bDiff = b.value.asArrayDiff!
                        let final = d1.timestamp > d2.timestamp ? bDiff + aDiff : aDiff + bDiff
                        return MySQLAttributeUpdate(attribute: a.attribute, value: .arrayDiff(final))
                    }
                } else {
                    return d1.timestamp > d2.timestamp ? a : b
                }
            }.map { $0.value }
            return MySQLSubTransaction(className: d1.className, objectId: d1.objectId, timestamp: d1.timestamp > d2.timestamp ? d1.timestamp : d2.timestamp, updates: updates)
        }
        return cd
    }

    func reduce() -> Transaction {
        let ddR = reduceDeletions()
        let cdR = reduceCreations()
        let mdR = reduceModifications()
        let cd = cdR.filter { ddR[$0.key] == nil || ddR[$0.key]!.timestamp < $0.value.object.timestamp }
        let md = mdR.filter { ddR[$0.key] == nil || cd[$0.key] != nil}
        let dd = ddR.filter { !(cdR[$0.key] != nil && cdR[$0.key]!.object.timestamp <= $0.value.timestamp) }
        return MySQLTransaction(deletions: dd.map { $0.value }, creations: cd.map { $0.value }, modifications: md.map { $0.value })
    }
    typealias SplittedTransaction = (deletion: SubTransaction?, creation: SubTransaction?, modification: SubTransaction?)

    func splitByObjects() -> [MySQLObjectDescription: SubtransactionType] {
        let dd = reduceDeletions()
        let cd = reduceCreations()
        let md = reduceModifications()
        let keys = Set(dd.keys).union(cd.keys).union(md.keys)
        var res = [MySQLObjectDescription: SubtransactionType]()
        keys.forEach { key in
            let ds = dd[key]
            let cs = cd[key]
            let ms = md[key]
            switch (ds, cs ,ms) {
            case let (d?, nil, nil):
                res[key] = .deletion(d)
            case let (nil, c?, nil):
                res[key] = .creation(c)
            case let (nil, nil, m?):
                res[key] = .modification(m)
            case let (d?, c?, nil) where d.timestamp <= c.object.timestamp:
                if !c.object.updates.isEmpty {
                    res[key] = .modification(c.object)
                }
            case let (nil, c?, m?) where c.object.timestamp <= m.timestamp:
                res[key] = .createModif(c: c, m: m)
            case let (d?, nil, m?) where m.timestamp <= d.timestamp:
                res[key] = .deletion(d)
            case let (d?, c?, m?) where d.timestamp <= c.object.timestamp && c.object.timestamp <= m.timestamp:
                let upsC = c.object.updates.map { MySQLHashableAttributeUpdate(update: $0) }
                let upsM = m.updates.map { MySQLHashableAttributeUpdate(update: $0) }
                let ups = Set(upsM).union(upsC).map { $0.update }
                res[key] = .modification(MySQLSubTransaction(className: key.className, objectId: key.objectId, timestamp: m.timestamp, updates: ups))
            case let (d?, c?, m?) where c.object.timestamp <= m.timestamp && m.timestamp <= d.timestamp:
                res[key] = .deletion(d)
            case let (d?, c?, m?) where m.timestamp <= d.timestamp && d.timestamp <= c.object.timestamp:
                res[key] = .modification(MySQLSubTransaction(className: key.className, objectId: key.objectId, timestamp: m.timestamp, updates: c.object.updates))
            default:
                break
            }
        }
        return res
    }

    func adding(_ rhs: Transaction) -> Transaction {
        MySQLTransaction(
            deletions: self.deletions + rhs.deletions,
            creations: self.creations + rhs.creations,
            modifications: self.modifications + rhs.modifications).reduce()
    }
}

enum SubtransactionType {
    case creation(CreationDescription)
    case deletion(SubTransaction)
    case modification(SubTransaction)
    case createModif(c: CreationDescription, m: SubTransaction)
}

struct  MySQLDiffGroupUpdate: DiffGroupUpdate {
    let group: Group

    let newObjects: [ObjectDescription]

    let role: Role
}

extension Array where Element: Update {
    func add(_ rhs: [Update]) -> [Update] {
        let upsC = self.map { MySQLHashableAttributeUpdate(update: $0) }
        let upsM = rhs.map { MySQLHashableAttributeUpdate(update: $0) }
        let ups = Set(upsM).union(upsC).map { $0.update }
        return ups
    }
}

extension MySQLDiffGroupUpdate: Hashable {
    static func == (lhs: MySQLDiffGroupUpdate, rhs: MySQLDiffGroupUpdate) -> Bool {
        let g1 = lhs.group as! MySQLGroup
        let g2 = rhs.group as! MySQLGroup
        let o1 = Set(lhs.newObjects.map { $0.className + $0.objectId })
        let o2 = Set(rhs.newObjects.map { $0.className + $0.objectId })
        let gOk = g1 == g2
        let oOk = o1 == o2
        let roleOk = lhs.role == rhs.role

        guard gOk && oOk && roleOk else {
            return false
        }
        return true
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(group as! MySQLGroup)
        hasher.combine(role)
        hasher.combine(Set(newObjects.map { $0.className + $0.objectId }))
    }
}

struct MySQLSubTransaction: SubTransaction {
    let className: String

    let objectId: ObjectID

    let timestamp: Date

    let updates: [Update]

}

extension MySQLSubTransaction: Hashable {
    static func == (lhs: MySQLSubTransaction, rhs: MySQLSubTransaction) -> Bool {
        let us1 = lhs.updates as! [MySQLAttributeUpdate]
        let us2 = rhs.updates as! [MySQLAttributeUpdate]
        let s1 = Set(us1)
        let s2 = Set(us2)
        let upOk = s1.subtracting(s1).union(s2.subtracting(s1)).isEmpty
        let classOk = lhs.className == rhs.className
        let objectOk = lhs.objectId == rhs.objectId
        let timestampOk = Int(lhs.timestamp.timeIntervalSince1970 * 1000) == Int(rhs.timestamp.timeIntervalSince1970 * 1000)
        let ok = upOk && classOk && objectOk && timestampOk
        guard ok else {
            return ok
        }
        return ok
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(className)
        hasher.combine(objectId)
        hasher.combine(Int(timestamp.timeIntervalSince1970 * 1000))
//        hasher.combine(updates as! [MySQLAttributeUpdate])
    }
}


struct MySQLCreationDescription: CreationDescription {
    let groupID: GroupID

    let object: SubTransaction
}

extension MySQLCreationDescription: Hashable {
    static func == (lhs: MySQLCreationDescription, rhs: MySQLCreationDescription) -> Bool {
        let st1 = lhs.object as! MySQLSubTransaction
        let st2 = rhs.object as! MySQLSubTransaction
        let ok = lhs.groupID == rhs.groupID && st1 == st2
        guard ok else {
            return ok
        }
        return ok
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(groupID)
        let st = object as! MySQLSubTransaction
        hasher.combine(st)
    }
}

struct MySQLAttributeUpdate: Update {
    var attribute: String

    var value: UpdateValue
}

extension MySQLAttributeUpdate: Hashable {
    static func == (lhs: MySQLAttributeUpdate, rhs: MySQLAttributeUpdate) -> Bool {
        let aOk = lhs.attribute == rhs.attribute
        let vOk = "\(lhs.value)" == "\(rhs.value)"
        let ok = aOk && vOk
        guard ok else {
            return ok
        }
        return ok
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(attribute)
        hasher.combine("\(value)")
    }
}

struct MySQLHashableAttributeUpdate {
    let update: Update
}

extension MySQLHashableAttributeUpdate: Hashable {
    static func == (lhs: MySQLHashableAttributeUpdate, rhs: MySQLHashableAttributeUpdate) -> Bool {
        return lhs.update.attribute == rhs.update.attribute
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(update.attribute)
    }
}


struct MySQLObjectDescription: ObjectDescription {
    let className: String

    let objectId: ObjectID

    init(className: String, objectId: ObjectID) {
        self.className = className
        self.objectId = objectId
    }

    init(subtransaction: SubTransaction) {
        className = subtransaction.className
        objectId = subtransaction.objectId
    }

    init(createSubtransaction: CreationDescription) {
        className = createSubtransaction.object.className
        objectId = createSubtransaction.object.objectId
    }
}

extension MySQLObjectDescription: Hashable {
    static func == (lhs: MySQLObjectDescription, rhs: MySQLObjectDescription) -> Bool {
        return lhs.className == rhs.className && lhs.objectId == rhs.objectId
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(className)
        hasher.combine(objectId)
    }
}

extension UpdateValue.ArrayDiff {
    static func +(_ lhs: UpdateValue.ArrayDiff, _ rhs: UpdateValue.ArrayDiff) -> UpdateValue.ArrayDiff {
        let lhsApplied = lhs.original.applying(lhs.difference)!
        let rhsApplied = rhs.original.applying(rhs.difference)!
        if lhsApplied == rhs.original {
            return .init(original: lhs.original, difference: rhsApplied.difference(from: lhs.original))
        } else if lhs.original == rhs.original {
            return .init(original: lhs.original, difference: lhs.difference + rhs.difference)
        } else {
            assertionFailure()
            return .init(original: lhsApplied, new: rhsApplied)
        }
    }
}

extension UpdateValue {
    var asArrayDiff: UpdateValue.ArrayDiff? {
        switch self {
        case let .array(ids):
            return .init(original: [], new: ids)
        case let .arrayDiff(ad):
            return ad
        default:
            return nil
        }
    }


    var isArray: Bool {
        asArrayDiff != nil
    }
}
