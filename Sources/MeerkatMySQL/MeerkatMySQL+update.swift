//
//  MeerkatMySQL+update.swift
//  
//
//  Created by Filip Klembara on 27/02/2020.
//

extension MeerkatMySQL {
    private func createNewObjects(object: MySQLObjectDescription, subtransaction: SubtransactionType, validGroups: Future<Set<GroupTable>>) -> Future<(MySQLObjectDescription, SubtransactionType)?> {
        let gg: Future<GroupTable?>
        switch subtransaction {
        case .createModif(let c, _), .creation(let c):
            gg = validGroups.flatMap { gs in
                guard gs.map({ $0.name }).contains(c.groupID) else { return self.mysql.future(nil) }
                return self.withDB { conn in GroupTable.query(on: conn).filter(\.name == c.groupID).first().unwrap(or: DBWrapperError.unknownGroup(id: c.groupID))
                    .flatMap { ObjectTable(className: c.object.className, objectId: c.object.objectId, groupId: $0.id!).create(on: conn).transform(to: $0) }
                }.map { $0 }
            }
        default:
            gg = self.getOptGroup(for: object)
        }
        return gg.and(validGroups).map { (g, gs) in
            g.flatMap { gs.contains($0) ? (object, subtransaction) : nil }
        }
    }

    private typealias ConflictDetection = (valid: [MySQLObjectDescription : SubtransactionType], new: [MySQLObjectDescription : SubtransactionType], confliting: [(MySQLObjectDescription, SubtransactionType, SubtransactionType)])
    private func detectConflicts(valid: [MySQLObjectDescription : SubtransactionType], new: [MySQLObjectDescription : SubtransactionType]) -> ConflictDetection {
        let keys = { () -> (valid: Set<MySQLObjectDescription>, new: Set<MySQLObjectDescription>, confliting: Set<MySQLObjectDescription>) in
            let vK = Set(valid.keys)
            let nK = Set(new.keys)
            return (vK.subtracting(nK), nK.subtracting(vK), vK.intersection(nK) )
        }()
        let validTransactions = valid.filter { keys.valid.contains($0.key) }
        let newTransactions = new.filter { keys.new.contains($0.key) }
        let conflictingTransactions = keys.confliting.map { ($0, valid[$0]!, new[$0]!) }
        return (validTransactions, newTransactions, conflictingTransactions)
    }

    private func resolveConflict(object: MySQLObjectDescription, valid: SubtransactionType, new: SubtransactionType) -> (MySQLObjectDescription, Resolution)? {
        let r: Resolution
        switch (valid, new) {
            case (.deletion, .deletion):
                return nil
            case let (.deletion(d1), .creation(c2)):
                r = d1.timestamp < c2.object.timestamp ? .toNew(.creation(c2)) : .toValid(.deletion(d1))
            case let (.deletion(d1), .modification):
                r = .toValid(.deletion(d1))
            case let (.deletion(d1), .createModif(c2, m2)):
                if d1.timestamp < m2.timestamp {
                    r = .toNew(.createModif(c: c2, m: m2))
                } else {
                    r = .toValid(.deletion(d1))
                }

            case let (.creation(c1), .deletion(d2)):
                r = c1.object.timestamp < d2.timestamp ? .toNew(.deletion(d2)) : .toValid(.creation(c1))
            case let (.creation(c1), .creation(c2)):
                r = self.interModif(fromValid: c1.object, fromNew: c2.object)
            case let (.creation(c1), .modification(m2)):
                r = self.interModif(fromValid: c1.object, fromNew: m2)
            case let (.creation(c1), .createModif(c2, m2)):
                r = self.interModif(fromValid: c1.object, fromNew: self.mergeUpdates(c2.object, newer: m2))

            case let (.modification, .deletion(d2)):
                    r = .toNew(.deletion(d2))
            case let (.modification(m1), .creation(c2)):
                r = self.interModif(fromValid: m1, fromNew: c2.object)
            case let (.modification(m1), .modification(m2)):
                r = self.interModif(fromValid: m1, fromNew: m2)
            case let (.modification(m1), .createModif(c2, m2)):
                r = self.interModif(fromValid: m1, fromNew: self.mergeUpdates(c2.object, newer: m2))

            case let (.createModif(c1, m1), .deletion(d2)):
                if m1.timestamp < d2.timestamp {
                    r = .toNew(.deletion(d2))
                } else {
                    r = .toValid(.createModif(c: c1, m: m1))
                }
            case let (.createModif(c1, m1), .creation(c2)):
                r = self.interModif(fromValid: self.mergeUpdates(c1.object, newer: m1), fromNew: c2.object)
            case let (.createModif(c1, m1), .modification(m2)):
                r = self.interModif(fromValid: self.mergeUpdates(c1.object, newer: m1), fromNew: m2)
            case let (.createModif(c1, m1), .createModif(c2, m2)):
                r = self.interModif(fromValid: self.mergeUpdates(c1.object, newer: m1), fromNew: self.mergeUpdates(c2.object, newer: m2))
        }
        return (object, r)
    }

    private func splitConflictDetection(detection: ConflictDetection) -> (valid: [MySQLObjectDescription: SubtransactionType], new: [MySQLObjectDescription: SubtransactionType]) {
        let resolvedConflicts = detection.confliting.map(self.resolveConflict).compactMap { $0 }

        let a = resolvedConflicts.map { k, r -> ([(MySQLObjectDescription, SubtransactionType)], [(MySQLObjectDescription, SubtransactionType)]) in
            switch r {
            case .toValid(let v):
                return ([(k, v)], [])
            case .toNew(let n):
                return ([], [(k, n)])
            case .to(let valid, let new):
                return ([(k, valid)], [(k, new)])
            }
        }
        let (valid, new) = a.reduce(([(MySQLObjectDescription, SubtransactionType)](), [(MySQLObjectDescription, SubtransactionType)]())) { a, b in
            (a.0 + b.0, a.1 + b.1)
        }
        let valids = detection.valid.merging(valid) { a, b in a }
        let news = detection.new.merging(new) { a, b in a }
        return (valids, news)
    }
    func splitByGroups(_ f:  EventLoopFuture<([MySQLObjectDescription : SubtransactionType])>) -> Future<[GroupTable : [SubtransactionType]]> {
        f.map { $0.mapValues { [$0] } }.innerFlatMapFlatten { k, v in
            self.getGroup(for: k).map { ($0, (k, v)) }
        }.map { $0.reduce([GroupTable: [SubtransactionType]]()) { d, v in
            var d = d
            d[v.0] = (d[v.0] ?? [SubtransactionType]()) + v.1.1
            return d
            }
        }
    }
    func splitByGroupsWithCreationRestoration(_ f:  EventLoopFuture<([MySQLObjectDescription : SubtransactionType])>) -> Future<[GroupTable : [SubtransactionType]]> {
        f.innerFlatMapFlatten { k, v -> Future<(GroupTable, (MySQLObjectDescription, [SubtransactionType]))> in
            switch v {
            case .createModif(let c, _), .creation(let c):
                return self.withDB { MeerkatMySQL.getGroupFuture(by: c.groupID, on: $0) }.map { ($0, (k, [v])) }
            default:
                return self.getGroup(for: k).map { ($0, (k, [v])) }.catch { err in print("err", err, k, v) }
            }
        }.map { $0.reduce([GroupTable: [SubtransactionType]]()) { d, v in
            var d = d
            d[v.0] = (d[v.0] ?? [SubtransactionType]()) + v.1.1
            return d
            }
        }
    }

    private func getVersion(for group: GroupTable) -> Future<GroupVersion> {
        withDB { conn in try group.signatures.query(on: conn).max(\.version).mapIfError { _ in 0 } }
    }

    private func getRole(in group: GroupTable, for user: User) -> Future<Role> {
        withDB { conn in
            try group.users.query(on: conn).filter(\.username == user.id)
                .decode(RoleTable.self).first()
                .unwrap(or: DBWrapperError.userNotInGroup(user: user, group: MySQLGroup(id: group.name, version: 0)))
                .map { $0.role.asRole }
        }
    }
    private func storeLog(for st: SubTransaction, type: UpdateType, in signature: SignatureTable, on conn: MySQLConnection) -> Future<[LogTable]> {
        let getObject = self.withDB { conn in ObjectTable.query(on: conn).filter(\.className == st.className).filter(\.objectId == st.objectId).first().unwrap(or: DBWrapperError.unknownObject(id: st.objectId)) }
        let log = {(objectTableId: Int) -> Future<[LogTable]> in
            LogTable(timestamp: st.timestamp, updateType: type, objectId: objectTableId, signatureId: signature.id!).create(on: conn)
                .flatMap { l -> Future<[LogTable]> in
                    let updates = try st.updates.map { u -> Future<String?> in
                        if u.value.isArray {
                            let mostRecentArray = getObject.flatMap { object -> Future<[String]> in
                                let attr = try object.logs.query(on: conn).join(\AttributeTable.logId, to: \LogTable.id).filter(\AttributeTable.key == u.attribute).sort(\.timestamp, .descending).decode(AttributeTable.self).first()
                                let value = attr.map { attr -> UpdateValue in
                                    guard let a = attr?.value else {
                                        return .array([])
                                    }
                                    return try self.scheme.getValue(for: u.attribute, from: a, in: st.className, prefferedArrayRepresentation: .arrayDiff)
                                }
                                return value.map { v -> [String] in
                                    guard let diff = v.asArrayDiff else {
                                        return []
                                    }
                                    return diff.original.applying(diff.difference) ?? []
                                }
                            }
                            let arrDiff = mostRecentArray.map { orig -> UpdateValue.ArrayDiff in
                                guard let valid = u.value.asArrayDiff!.original.applying(u.value.asArrayDiff!.difference) else {
                                    return .init(original: orig, new: orig)
                                }
                                return .init(original: orig, new: valid)
                            }
                            let store = arrDiff.flatMap {
                                AttributeTable(key: u.attribute, value: try self.scheme.getData(for: u.attribute, value: .arrayDiff($0), in: st.className), logId: l.id!).create(on: conn)
                            }
                            return store.transform(to: nil)
                        } else {
                            return AttributeTable(key: u.attribute, value: try self.scheme.getData(for: u.attribute, value: u.value, in: st.className), logId: l.id!).create(on: conn).transform(to: u.attribute)
                        }
                    }.flatten(on: conn).map { $0.compactMap { $0 } }
                    guard self.reduceAttributesHistory else { return updates.transform(to: [l]) }
                    let obj = self.withDB { l.object.get(on: $0) }
                    let attrsToDelete = updates.and(obj).flatMap { attrs, obj -> Future<[AttributeTable]> in
                        var q = try obj.logs.query(on: conn).filter(\.id != l.id!).join(\AttributeTable.logId, to: \LogTable.id)
                        if l.updateType != .deletion {
                            q = q.filter(\AttributeTable.key ~~ attrs)
                        }
                        return q.decode(AttributeTable.self).all()
                    }
                    let attrDelete = attrsToDelete.flatMap { attrs -> Future<Void> in
                        guard !attrs.isEmpty else { return self.mysql.future() }
                        let logsIds = attrs.map { $0.logId }
                        let attrsIds = attrs.map { $0.id! }
                        let del = AttributeTable.query(on: conn).filter(\.id ~~ attrsIds).delete()
                        let logs = del.flatMap { LogTable.query(on: conn).filter(\.id ~~ logsIds).filter(\.updateType != .creation).all() }
                        let logsToDelete = logs.innerFlatMapFlatten { log -> Future<LogTable?> in
                            let attrsCount = try log.attributes.query(on: conn).count()
                            return attrsCount.map(to: Optional<LogTable>.self) { $0 > 0 ? nil : log }
                        }.map { $0.compactMap { $0 } }
                        let logsDelete = logsToDelete.flatMap { logs -> Future<Void> in
                            guard !logs.isEmpty else { return self.mysql.future() }
                            let sigsIds = logs.map { $0.signatureId }
                            let logsIds = logs.map { $0.id! }
                            let del = LogTable.query(on: conn).filter(\.id ~~ logsIds).delete()
                            let sigs = del.flatMap { SignatureTable.query(on: conn).filter(\.id ~~ sigsIds).all() }
                            let sigsToDelete = sigs.innerFlatMapFlatten { sig -> Future<SignatureTable?> in
                                try sig.logs.query(on: conn).count().map { $0 > 0 ? nil : sig }
                            }.map { $0.compactMap { $0 } }
                            let sigsDelete = sigsToDelete.flatMap { sigs -> Future<Void> in
                                guard !sigs.isEmpty else { return self.mysql.future() }
                                let sigsIds = sigs.map { $0.id! }
                                let del = SignatureTable.query(on: conn).filter(\.id ~~ sigsIds).delete()
                                return del
                            }
                            return sigsDelete
                        }
                        return logsDelete
                    }
                    return attrDelete.transform(to: [l])
            }
        }
        return getObject.flatMap { log($0.id!) }.mapIfError { _ in []}
    }

//    private func reduceLogs

    private func applyValidSubTransactions(in group: GroupTable, subtransaction: [SubtransactionType], on conn: MySQLConnection) -> Future<MySQLGroup> {
        let getSignature = self.getVersion(for: group).flatMap { v in SignatureTable(version: v + 1, groupId: group.id!).create(on: conn) }

        let getLogs = getSignature.flatMap { signature -> Future<SignatureTable> in
            return subtransaction.map { s -> Future<[LogTable]> in

                switch s {
                case .creation(let c):
                    return self.storeLog(for: c.object, type: .creation, in: signature, on: conn)
                case .deletion(let d):
                    return self.storeLog(for: d, type: .deletion, in: signature, on: conn)
                case .modification(let m):
                    return self.storeLog(for: m, type: .modification, in: signature, on: conn)
                case .createModif(let c, let m):
                    return self.storeLog(for: c.object, type: .creation, in: signature, on: conn).flatMap { x in self.storeLog(for: m, type: .modification, in: signature, on: conn).map { x + $0 } }
                }
            }.flatten(on: conn).transform(to: signature)
        }
        return getLogs.map { MySQLGroup(id: group.name, version: $0.version) }
    }

    private func rollback(transactions: [MySQLObjectDescription : SubtransactionType], readGroups: Set<GroupTable>) -> Future<Transaction> {
        let filtered1 = transactions.filter { k, v in
            switch v {
            case .creation(let c), .createModif(let c, _):
                return readGroups.map { $0.name }.contains(c.groupID)
            default:
                return true
            }
        }
        let byGroups = filtered1.map { e -> Future<Transaction> in
            let g = getGroup(for: e.key).map { ($0, e.key, e.value) }
            let trans = g.flatMap { g, o, t -> Future<Transaction?> in
                guard readGroups.contains(g) else {
                    return self.mysql.future(nil)
                }
                let object = self.withDB { conn in
                    try g.objects.query(on: conn).filter(\.className == o.className).filter(\.objectId == o.objectId).first().unwrap(or: DBWrapperError.unknownObject(id: o.objectId))
                }
                let getAllSubtransactions = { (obj: ObjectTable) -> Future<Transaction> in
                    self.withDB { conn in
                        try obj.logs.query(on: conn).all().innerFlatMapFlatten { self.getSubtransaction(for: $0, on: conn) }.map {
                            self.mergeSubtransactions(groupId: g.name, subtrans: $0)
                        }
                    }
                }
                let objTrans = object.flatMap { obj -> Future<Transaction> in
                    return getAllSubtransactions(obj)
                }
                return objTrans.map { .some($0) }
            }.mapIfError { _ in MySQLTransaction() }
            let resolved = { trans.map { t -> Transaction in
                guard let t = t else {
                    return MySQLTransaction()
                }
                let s = MySQLSubTransaction(className: e.key.className, objectId: e.key.objectId, timestamp: Date.distantPast, updates: [])
                return MySQLTransaction(deletions: [s] + t.deletions, creations: t.creations, modifications: t.modifications)
                } }
            return resolved()
        }
        let merged = byGroups.flatten(on: self.mysql)
            .map { $0.reduce(MySQLTransaction() as Transaction) { $0.adding($1) } }
        return merged
    }

    public func update(groups: [Group], with transaction: Transaction, by user: User) -> Future<Diff> {
        let allGroups = getWritableGroups(from: groups, for: user)
        let readGroups = allGroups.map { $0.readable }
        let validGroups = allGroups.map { $0.writable }
        let transaction = transaction.reduce()
        let newDiff = diffs(for: user, in: groups)
        let newAllTransactions = newDiff.map { $0.transactions }
        let newReducedTransaction = newAllTransactions.map { $0.reduce(MySQLTransaction() as Transaction) { $0.adding($1) } }
        let newTransactionByObjects = newReducedTransaction.map { $0.splitByObjects() }
        let allValidTransactionsByObjects = transaction.splitByObjects()
        let rollbacked = readGroups.flatMap { self.rollback(transactions: allValidTransactionsByObjects, readGroups: $0) }.map {
            $0.splitByObjects()
        }

        let validTransactionByObjects = allValidTransactionsByObjects
            .map { self.createNewObjects(object: $0, subtransaction: $1, validGroups: validGroups) }
            .flatten(on: mysql)
            .map { Dictionary(uniqueKeysWithValues: $0.compactMap { $0 }) }

        let detectConfs = { validTransactionByObjects.and(newTransactionByObjects).map(self.detectConflicts) }

        let validsNews = detectConfs().map(self.splitConflictDetection)
        let getValidsByGroup = { self.splitByGroups(validsNews.map { $0.valid }) }
        let useValids = getValidsByGroup().innerFlatMapFlatten { (g, ot) -> Future<(Group, Role)> in
            self.withDB { self.applyValidSubTransactions(in: g, subtransaction: ot, on: $0).and(self.getRole(in: g, for: user)).map { ($0, $1) } }
        }
        let news = validsNews.map { $0.new }.and(rollbacked).map { n, r -> [MySQLObjectDescription : SubtransactionType] in
            var res = n
            r.forEach { res[$0.key] = $0.value }
            return res
        }
        let useNew = news.flatMap { ts -> Future<([DiffGroupUpdate], [Transaction])> in
            var gs = [GroupID: [ObjectDescription]]()
            var deletions = [SubTransaction]()
            var creations = [CreationDescription]()
            var modifications = [SubTransaction]()
            func add(_ o: ObjectDescription, to g: GroupID) {
                var os = gs[g] ?? []
                os.append(o)
                gs[g] = os
            }
            ts.forEach { st in
                switch st.value {
                case .creation(let c):
                    creations.append(c)
                    add(st.key, to: c.groupID)
                case .deletion(let d):
                    deletions.append(d)
                case .modification(let m):
                    modifications.append(m)
                case .createModif(let c, let m):
                    creations.append(c)
                    add(st.key, to: c.groupID)
                    modifications.append(m)
                }
            }
            let trans = MySQLTransaction(deletions: deletions, creations: creations, modifications: modifications)
            let getDiffGroups = gs.map { gid, os -> Future<DiffGroupUpdate> in
                let g = self.withDB { MeerkatMySQL.getGroupFuture(by: gid, on: $0) }
                let r = g.flatMap { self.getRole(in: $0, for: user) }
                let v = g.flatMap { self.getVersion(for: $0) }
                let mg = v.map { MySQLGroup(id: gid, version: $0) }
                return mg.and(r).map { MySQLDiffGroupUpdate(group: $0, newObjects: os, role: $1) }
            }.flatten(on: self.mysql)
            return getDiffGroups.map { ($0, [trans]) }
        }
        let partialDiff = useValids.flatMap { vsgr -> Future<Diff> in
            useNew.map { gs, ts -> Diff in
                let vg = Dictionary(uniqueKeysWithValues: vsgr.map { ($0.0.id, MySQLDiffGroupUpdate(group: $0.0, newObjects: [], role: $0.1)) })
                let ns = Dictionary(uniqueKeysWithValues: gs.map { ($0.group.id, $0) })
                let ss = ns.merging(vg) { a, b -> MySQLDiffGroupUpdate in
                    let v = a.group.version > b.group.version ? a.group.version : b.group.version
                    let no = a.newObjects + b.newObjects
                    return MySQLDiffGroupUpdate(group: MySQLGroup(id: a.group.id, version: v), newObjects: no, role: a.role)
                }
                return MySQLDiff(groups: ss.map { $0.value }, transactions: ts)
            }
        }
        let resultDiff = newDiff.and(validGroups.and(readGroups).map { Set($0.map { $0.name }).union($1.map { $0.name }) }).and(partialDiff).map { arg1, partDiff -> Diff in
            let (newDiff, validGroups) = arg1
            let resultGroups = Set(partDiff.groups.map { $0.group.id })
            let notHandledGroups = newDiff.groups.filter { !resultGroups.contains($0.group.id) && validGroups.contains($0.group.id) }
            notHandledGroups.forEach { assert($0.newObjects.isEmpty) }
            let trans = partDiff.transactions.flatMap { t -> [Transaction] in
                guard !t.creations.isEmpty || !t.modifications.isEmpty || !t.deletions.isEmpty else {
                    return []
                }
                return [t.reduce()]
            }
            return MySQLDiff(groups: partDiff.groups + notHandledGroups, transactions: trans)
        }
        return resultDiff
    }


    private enum Resolution {
        case toValid(SubtransactionType)
        case toNew(SubtransactionType)
        case to(valid: SubtransactionType, new: SubtransactionType)
    }

    private func interModif(fromValid v: SubTransaction, fromNew n: SubTransaction) -> Resolution {
        let vu = Dictionary(uniqueKeysWithValues: v.updates.map { (MySQLHashableAttributeUpdate(update: $0), $0) })
        let nu = Dictionary(uniqueKeysWithValues: n.updates.map { (MySQLHashableAttributeUpdate(update: $0), $0) })
        let vuk = Set(vu.keys)
        let nuk = Set(nu.keys)
        let vk = vuk.subtracting(nuk)
        let nk = nuk.subtracting(vuk)
        let ck = vuk.intersection(nuk)
        var vt = vk.map { vu[$0]! }
        var nt = nk.map { nu[$0]! }
        ck.map { (vu[$0]!, nu[$0]!) }.forEach { vuu, nuu in
            if vuu.value.isArray && nuu.value.isArray {
                let nArrayDiff = nuu.value.asArrayDiff!
                let vArrayDiff = vuu.value.asArrayDiff!
                let orig = nArrayDiff.original
                let vDiff = vArrayDiff.original.applying(vArrayDiff.difference)!.difference(from: orig)
                let nDiff = nArrayDiff.difference
                let combinedDiff = v.timestamp >= n.timestamp ? nDiff + vDiff : vDiff + nDiff
                let combinedArray = orig.applying(combinedDiff)!
                let newN = MySQLAttributeUpdate(attribute: vuu.attribute, value: .arrayDiff(.init(original: vArrayDiff.original, new: combinedArray)))
                let newV = MySQLAttributeUpdate(attribute: nuu.attribute, value: .arrayDiff(.init(original: nArrayDiff.original, new: combinedArray)))
                vt.append(newV)
                nt.append(newN)
            } else {
                if v.timestamp >= n.timestamp {
                    vt.append(vuu)
                } else {
                    nt.append(nuu)
                }
            }
        }
        let newValid = MySQLSubTransaction(className: v.className, objectId: v.objectId, timestamp: v.timestamp, updates: vt)
        let newNew = MySQLSubTransaction(className: n.className, objectId: n.objectId, timestamp: n.timestamp, updates: nt)
        if vt.isEmpty {
            return .toNew(.modification(newNew))
        } else if nt.isEmpty {
            return .toValid(.modification(newValid))
        } else {
            return .to(valid: .modification(newValid), new: .modification(newNew))
        }
    }

    private func mergeUpdates(_ u1: SubTransaction, newer u2: SubTransaction) -> SubTransaction {
        let upsC = u1.updates.map { MySQLHashableAttributeUpdate(update: $0) }
        let upsM = u2.updates.map { MySQLHashableAttributeUpdate(update: $0) }
        let ups = Set(upsM).union(upsC).map { $0.update }
        return MySQLSubTransaction(className: u2.className, objectId: u2.objectId, timestamp: u2.timestamp, updates: ups)
    }
    private func getWritableGroups(from groups: [Group], for user: User) -> Future<(readable: Set<GroupTable>, writable: Set<GroupTable>)> {
        let allGroups = withDB { conn -> EventLoopFuture<[(GroupTable, RoleTable)]> in
            let getUser = MeerkatMySQL.getUserFuture(by: user.id, on: conn)
            let getGroups = getUser.flatMap { user -> Future<[(GroupTable, RoleTable)]> in
                try user.groups.query(on: conn).filter(\.name ~~ groups.map { $0.id }).alsoDecode(RoleTable.self).all()
            }
            return getGroups
        }
        let splitted = allGroups.map { groups -> (readable: Set<GroupTable>, writable: Set<GroupTable>) in
            var readable = Set<GroupTable>()
            var writable = Set<GroupTable>()
            groups.forEach { group, role in
                if role.role.asRole == .read {
                    readable.insert(group)
                } else {
                    writable.insert(group)
                }
            }
            return (readable: readable, writable: writable)
        }
        return splitted
    }

    private func getOptGroup(for object: ObjectDescription) -> Future<GroupTable?> {
        let obj = withDB { conn in ObjectTable.query(on: conn).filter(\.className == object.className).filter(\.objectId == object.objectId).join(\GroupTable.id, to: \ObjectTable.groupId).decode(GroupTable.self).first()}
        return obj
    }

    private func getGroup(for object: ObjectDescription) -> Future<GroupTable> {
        return getOptGroup(for: object).unwrap(or: DBWrapperError.unknownObject(id: object.objectId))
    }
}
