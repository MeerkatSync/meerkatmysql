//
//  SignatureTable.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 23/01/2020.
//

struct SignatureTable: MySQLModel {
    var id: Int?
    let version: GroupVersion

    let groupId: GroupTable.ID

    static var name = "sync_signature"
    static var entity = "sync_signatures"
}

extension SignatureTable {
    var group: Parent<SignatureTable, GroupTable> {
        return parent(\.groupId)
    }

    var logs: Children<SignatureTable, LogTable> {
        return children(\.signatureId)
    }
}

extension SignatureTable: MySQLMigration { }

