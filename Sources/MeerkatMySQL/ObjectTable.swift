//
//  ObjectTable.swift
//  Async
//
//  Created by Filip Klembara on 26/01/2020.
//

struct ObjectTable: MySQLModel {
    var id: Int?
    let className: String
    let objectId: ObjectID

    let groupId: GroupTable.ID

    static var name = "sync_object"
    static var entity = "sync_objects"
}

extension ObjectTable {
    var group: Parent<ObjectTable, GroupTable> {
        return parent(\.groupId)
    }

    var logs: Children<ObjectTable, LogTable> {
        return children(\.objectId)
    }
}

extension ObjectTable: MySQLMigration { }


