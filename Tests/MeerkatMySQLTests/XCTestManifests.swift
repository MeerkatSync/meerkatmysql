import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MeerkatMySQLTests.allTests),
        testCase(UnitTests.allTests)
    ]
}
#endif
