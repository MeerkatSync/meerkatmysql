//
//  MySQLGroup.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 24/01/2020.
//

struct MySQLGroup: Group {
    let id: GroupID

    let version: GroupVersion
}


extension MySQLGroup: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case version
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        version = try container.decodeIfPresent(GroupVersion.self, forKey: .version) ?? 0
        id = try container.decode(GroupID.self, forKey: .id)
    }
}

extension MySQLGroup: Hashable {
    public static func == (lhs: MySQLGroup, rhs: MySQLGroup) -> Bool {
        return lhs.id == rhs.id && lhs.version == rhs.version
    }

    public func hash(into hasher: inout Hasher) {
        id.hash(into: &hasher)
        version.hash(into: &hasher)
    }
}
