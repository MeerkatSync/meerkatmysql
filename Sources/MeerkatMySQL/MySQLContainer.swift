//
//  MySQLContainer.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 20/01/2020.
//

public class MySQLContainer: Container {

    public var config: Config
    public var environment: Environment
    public var services: Services
    public var serviceCache: ServiceCache = .init()
    private var eventLoopGroup: EventLoopGroup
    public var eventLoop: EventLoop {
        return eventLoopGroup.next()
    }

    public init(environment: Environment, config: Config, conf: MySQLDatabaseConfig, services: Services) throws {
        self.config = config
        self.environment = environment
        self.eventLoopGroup = MultiThreadedEventLoopGroup(numberOfThreads: 1)
        self.services = services
    }

    public func run() -> Future<Void> {
        boot()
    }

    private func boot() -> Future<Void> {
        return Future.flatMap(on: self) {
            // will-boot all service providers
            return try self.providers.map {
                try $0.willBoot(self)
            }.flatten(on: self)
        }.map {
            if _isDebugAssertConfiguration() && self.environment.isRelease {
                let log = try self.make(Logger.self)
                log.warning("Debug build mode detected while configured for release environment: \(self.environment.name).")
                log.info("Compile your application with `-c release` to enable code optimizations.")
            }
        }.flatMap {
            // did-boot all service providers
            return try self.providers.map {
                try $0.didBoot(self)
            }.flatten(on: self)
        }
    }

    deinit {
        try? eventLoopGroup.syncShutdownGracefully()
        guard let threadPool = try? make(BlockingIOThreadPool.self) else {
            return
        }
        try? threadPool.syncShutdownGracefully()
    }
}
