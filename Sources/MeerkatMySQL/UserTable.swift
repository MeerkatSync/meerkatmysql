//
//  UserTable.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 20/01/2020.
//

struct MySQLUser: User, Codable {
    var id: UserID
}

extension MySQLUser: Equatable {
    static func ==(_ lhs: MySQLUser, _ rhs: MySQLUser) -> Bool {
        return lhs.id == rhs.id
    }
}

struct UserTable: MySQLModel {

    var id: Int?
    let username: String

    static var name = "sync_user"
    static var entity = "sync_users"
}

extension UserTable {
    var groups: Siblings<UserTable, GroupTable, RoleTable> {
        return siblings()
    }

    var devices: Children<UserTable, DeviceTable> {
        return children(\.userId)
    }
}

extension UserTable: MySQLMigration { }
