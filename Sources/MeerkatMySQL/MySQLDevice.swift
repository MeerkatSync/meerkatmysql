//
//  MySQLDevice.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 08/02/2020.
//

struct MySQLDevice: Device {
    let id: String

    let description: String
}
