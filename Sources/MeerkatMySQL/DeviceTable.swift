//
//  DeviceTable.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 04/02/2020.
//

struct DeviceTable: MySQLModel {
    var id: Int?
    let device: String
    let info: String

    let userId: UserTable.ID

    static var name = "sync_device"
    static var entity = "sync_devices"
}

extension DeviceTable {
    var users: Parent<DeviceTable, UserTable> {
        return parent(\.userId)
    }
}

extension DeviceTable: MySQLMigration { }
