//
//  RoleTable.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 20/01/2020.
//

struct RoleTable: MySQLPivot {
    static var leftIDKey: LeftIDKey = \.user
    static var rightIDKey: RightIDKey = \.group

    typealias Left = UserTable
    typealias Right = GroupTable

    var id: Int?
    var user: UserTable.ID
    var group: GroupTable.ID
    var role: RoleType

    static var name = "sync_role"
    static var entity = "sync_roles"
}

enum RoleType: String, MySQLEnumType {
    static func reflectDecoded() throws -> (RoleType, RoleType) {
        return (.owner, .read)
    }

    init(role: Role) {
        switch role {
        case .owner:
            self = .owner
        case .readAndWrite:
            self = .readAndWrite
        case .read:
            self = .read
        }
    }

    case owner
    case readAndWrite
    case read

    var asRole: Role {
        switch self {
        case .owner:
            return .owner
        case .readAndWrite:
            return .readAndWrite
        case .read:
            return .read
        }
    }
}

extension RoleTable: MySQLMigration { }
