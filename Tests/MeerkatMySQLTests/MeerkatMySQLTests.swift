import XCTest
@testable import MeerkatMySQL
import FluentMySQL
import MeerkatCore
import MeerkatServerCore
import MeerkatSchemeDescriptor

private let refDate = Date()

final class MeerkatMySQLTests: XCTestCase {
    lazy var conf: MySQLDatabaseConfig = {
        guard let pw = ProcessInfo.processInfo.environment["MYSQL_ROOT_PASSWORD"],
            let user = ProcessInfo.processInfo.environment["C_MYSQL_USERNAME"],
            let host = ProcessInfo.processInfo.environment["C_MYSQL_HOST"],
            let sPort = ProcessInfo.processInfo.environment["C_MYSQL_PORT"],
            let port = Int(sPort),
            let db = ProcessInfo.processInfo.environment["MYSQL_DATABASE"] else {

                fatalError()
        }
        let conf = MySQLDatabaseConfig(hostname: host, port: port, username: user, password: pw, database: db, transport: .unverifiedTLS)
        return conf
    }()

    override func setUp() {
        let d1 = Date()
        dropDB()
        let container = getContainer()
        let (u1, g1) = createUser(id: "A", container: container)
        let (u2, g2) = createUser(id: "B", container: container)
        let (u3, g3) = createUser(id: "C", container: container)
        let g12 = try! container.withPooledConnection(to: .mysql) { con in
            GroupTable(name: "groupAB").save(on: con).then { g in RoleTable(user: u1.id!, group: g.id!, role: .owner).save(on: con).map { _ in g } }
                .then { g in RoleTable(user: u2.id!, group: g.id!, role: .read).save(on: con).map { _ in g } }
        }.wait()
        _ = try! container.withPooledConnection(to: .mysql) { conn in
            DeviceTable(device: "deviceA1", info: "iOS", userId: u1.id!).save(on: conn)
                .transform(to: DeviceTable(device: "deviceB1", info: "iOS", userId: u2.id!).save(on: conn))
                .transform(to: DeviceTable(device: "deviceB2", info: "iOS", userId: u2.id!).save(on: conn))
        }.wait()
        let oA = createObject(className: "Person", objectId: "oA1", in: g1, container: container)
        let oB = createObject(className: "Person", objectId: "oB1", in: g2, container: container)
//        let oC = createObject(className: "Person", objectId: "oC1", in: g3, container: container)
        let oAB1 = createObject(className: "Person", objectId: "oAB1", in: g12, container: container)
        let oAB2 = createObject(className: "Person", objectId: "oAB2", in: g12, container: container)
        let oAB3 = createObject(className: "Person", objectId: "oAB3", in: g12, container: container)
        _ = try! container.withPooledConnection(to: .mysql) { con -> EventLoopFuture<Void> in
            let updateAB1 = SignatureTable(version: 1, groupId: g12.id!).save(on: con).flatMap { signature -> EventLoopFuture<SignatureTable> in
                let p1 = createPersonLog(name: "Mark", age: 21, timestamp: refDate - 3800, object: oAB1, signature: signature, on: con)
                let p2 = createPersonLog(name: "Tom", age: 23, timestamp: refDate - 3500, object: oAB2, signature: signature, on: con)
                return p1.and(p2).transform(to: signature)
            }
            let updateA1 = SignatureTable(version: 1, groupId: g1.id!).save(on: con).flatMap { signature -> EventLoopFuture<SignatureTable> in
                let p1 = createPersonLog(name: "A", age: 15, timestamp: refDate - 2512, object: oA, signature: signature, on: con)
                return p1.transform(to: signature)
            }
            let updateAB2 = SignatureTable(version: 2, groupId: g12.id!).save(on: con).flatMap { signature -> EventLoopFuture<SignatureTable> in
                let p1 = createPersonLog(name: "Lucy", age: 22, timestamp: refDate - 1824, object: oAB3, signature: signature, on: con)
                let p2 = modifyPersonLog(age: 24, timestamp: refDate - 3000, object: oAB2, signature: signature, on: con)
                return p1.and(p2).transform(to: signature)
            }
            let updateA2 = SignatureTable(version: 2, groupId: g1.id!).save(on: con).flatMap { signature -> EventLoopFuture<SignatureTable> in
                let p1 = modifyPersonLog(age: 20, timestamp: refDate - 2511, object: oA, signature: signature, on: con)
                return p1.transform(to: signature)
            }
            let updateAB3 = SignatureTable(version: 3, groupId: g12.id!).save(on: con).flatMap { signature -> EventLoopFuture<SignatureTable> in
                let p1 = deletePersonLog(timestamp: refDate - 1015, object: oAB1, signature: signature, on: con)
                let p2 = modifyPersonLog(age: 25, timestamp: refDate - 1200, object: oAB2, signature: signature, on: con)
                return p1.and(p2).transform(to: signature)
            }
            let updateB1 = SignatureTable(version: 1, groupId: g2.id!).save(on: con).flatMap { signature -> EventLoopFuture<SignatureTable> in
                let p1 = createPersonLog(name: "B", age: 63, timestamp: refDate - 100, object: oB, signature: signature, on: con)
                return p1.transform(to: signature)
            }
            let updates = [updateAB1.transform(to: updateAB2).transform(to: updateAB3), updateA1.transform(to: updateA2), updateB1]
            return updates.flatten(on: con).transform(to: ())
        }.wait()
        let d2 = Date()
        print("setup", d2.timeIntervalSince(d1))
    }

    func setUpContainer(_ config: inout Config, _ env: inout Environment, _ services: inout Services) {
        let cls = ClassAttributesDescription(className: "Person", attributes: ["name": .string, "age": .uint])
        let scheme = MeerkatSchemeDescriptor(scheme: [cls])

        services.register(scheme)

        let p = MySQLDatabase(config: conf)
        var databases = DatabasesConfig()
        databases.add(database: p, as: .mysql)
        services.register(databases)
    }

    private lazy var container: Container = {
        var config = Config.meerkatMySQLDefault
        var env = Environment.testing
        var services = Services.meerkatMySQLDefault
        setUpContainer(&config, &env, &services)
        let container = try! MySQLContainer(environment: env, config: config, conf: conf, services: services)
        try! container.run().wait()
        return container
    }()

    func getContainer() -> Container {
        return container
    }

    func getMeerkatMySQL(from container: Container) -> MeerkatMySQL {
        try! container.make(MeerkatMySQL.self)
    }

    private func dropDB() {
        let container = getContainer()
        try! container.withPooledConnection(to: .mysql) { DeviceTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { AttributeTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { LogTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { SignatureTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { ObjectTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { RoleTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { GroupTable.query(on: $0).delete(force: true) }.wait()
        try! container.withPooledConnection(to: .mysql) { UserTable.query(on: $0).delete(force: true) }.wait()
    }

    override func tearDown() {
        let d1 = Date()
        dropDB()
        let d2 = Date()
        print("teardown", d2.timeIntervalSince(d1))
    }

    func testUser() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let _user = try? mysql.user(by: "A").wait()
        XCTAssertNotNil(_user)

        guard let user = _user else {
            XCTFail()
            return
        }

        XCTAssertEqual(user.id, "A")
    }

    func testGroupsA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let _user = mysql.user(by: "A")

        guard let user = try? _user.wait() else {
            XCTFail()
            return
        }

        guard let a = (try? mysql.groups(for: user).wait()) as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp = [MySQLGroup(id: "groupA", version: 2), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(a), Set(exp))
    }

    func testGroupsB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let _user = mysql.user(by: "B")
        guard let user = try? _user.wait() else {
            XCTFail()
            return
        }
        guard let a = (try? mysql.groups(for: user).wait()) as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp = [MySQLGroup(id: "groupB", version: 1), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(a), Set(exp))
    }

    func testGroupsC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let _user = try? mysql.user(by: "C").wait()
        guard let user = _user else {
            XCTFail()
            return
        }
        guard let a = (try? mysql.groups(for: user).wait()) as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 1, "\(a)")
        let exp = [MySQLGroup(id: "groupC", version: 0)]
        XCTAssertEqual(Set(a), Set(exp))
    }

    func testGroupA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        guard let group = try? mysql.group(by: "groupA").wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(group.id, "groupA")
        XCTAssertEqual(group.version, 2)
    }

    func testGroupB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        guard let group = try? mysql.group(by: "groupB").wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(group.id, "groupB")
        XCTAssertEqual(group.version, 1)
    }

    func testGroupC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        guard let group = try? mysql.group(by: "groupC").wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(group.id, "groupC")
        XCTAssertEqual(group.version, 0)
    }

    func testGroupAB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        guard let group = try? mysql.group(by: "groupAB").wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(group.id, "groupAB")
        XCTAssertEqual(group.version, 3)
    }

    func testAddGroup() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        XCTAssertNil(mysql.group(by: "groupC2").get)

        let _user = mysql.user(by: "C")
        guard let user = try?_user.wait() else {
            XCTFail()
            return
        }

        guard let g = try? mysql.addGroup(withId: "groupC2", owner: user, objects: []).wait() else {
            XCTFail()
            return
        }

        XCTAssertEqual(g.id, "groupC2")
        XCTAssertEqual(g.version, 0)

        guard let group = try? mysql.group(by: "groupC2").wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(group.id, "groupC2")
        XCTAssertEqual(group.version, 0)

        guard let a = try? mysql.groups(for: user).wait() as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp = [MySQLGroup(id: "groupC", version: 0), MySQLGroup(id: "groupC2", version: 0)]
        XCTAssertEqual(Set(a), Set(exp))
    }

    func testSubscribeToGroup() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _cuser = mysql.user(by: "C")
        guard let userC = _cuser.get else {
            XCTFail()
            return
        }

        guard let a = mysql.groups(for: userC).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 1, "\(a)")
        let exp1 = [MySQLGroup(id: "groupC", version: 0)]
        XCTAssertEqual(Set(a), Set(exp1))

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        XCTAssertTrue(mysql.subscribe(MySQLUser(id: "C"), to: MySQLGroup(id: "groupAB", version: 0), by: userA, as: .read).getBool)

        guard let b = mysql.groups(for: userC).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(b.count, 2, "\(b)")
        let exp2 = [MySQLGroup(id: "groupC", version: 0), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(b), Set(exp2))
    }

    func testUnsubscribeFromGroup() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B").asResult
        XCTAssertNil(_buser.asFailure)
        guard let userB = _buser.asSuccess else {
            XCTFail()
            return
        }
        let _auser = mysql.user(by: "A").asResult
        XCTAssertNil(_auser.asFailure)
        guard let userA = _auser.asSuccess else {
            XCTFail()
            return
        }

        let getGroupB = mysql.groups(for: userB).asResult
        XCTAssertNil(getGroupB.asFailure)
        guard let a = getGroupB.asSuccess as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp1 = [MySQLGroup(id: "groupB", version: 1), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(a), Set(exp1))

        let query1 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupAB").first()
                .unwrap(or: DBWrapperError.unknownGroup(id: "groupAB")).thenThrowing { try $0.objects.query(on: conn).count() }.flatMap { $0 }
        }
        guard let c1 = try? query1.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(c1, 3)

        XCTAssertTrue(mysql.unsubscribe(MySQLUser(id: "B"), from: MySQLGroup(id: "groupAB", version: 0), by: userA).getBool)

        guard let b = mysql.groups(for: userB).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(b.count, 1, "\(b)")
        let exp2 = [MySQLGroup(id: "groupB", version: 1)]
        XCTAssertEqual(Set(b), Set(exp2))
        let query2 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupAB").first()
                .unwrap(or: DBWrapperError.unknownGroup(id: "groupAB")).thenThrowing { try $0.objects.query(on: conn).count() }.flatMap { $0 }
        }
        guard let c2 = try? query2.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(c2, 3)
    }
    func testUnsubscribeWrongUserFromGroup() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A").asResult
        XCTAssertNil(_auser.asFailure)
        guard let userA = _auser.asSuccess else {
            XCTFail()
            return
        }

        let getGroupB = mysql.groups(for: userA).asResult
        XCTAssertNil(getGroupB.asFailure)
        guard let a = getGroupB.asSuccess as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp1 = [MySQLGroup(id: "groupA", version: 2), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(a), Set(exp1))

        let query1 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupAB").first()
                .unwrap(or: DBWrapperError.unknownGroup(id: "groupAB")).thenThrowing { try $0.objects.query(on: conn).count() }.flatMap { $0 }
        }
        guard let c1 = try? query1.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(c1, 3)

        let d = mysql.unsubscribe(MySQLUser(id: "C"), from: MySQLGroup(id: "groupAB", version: 0), by: userA)
        XCTAssertNil(d.asResult.asFailure)
        XCTAssertNotNil(d.asResult.asSuccess)
        XCTAssertTrue(d.getBool)
    }

    func testUnsubscribeFromGroupAndRemove() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }

        guard let a = mysql.groups(for: userB).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp1 = [MySQLGroup(id: "groupB", version: 1), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(a), Set(exp1))

        let query1 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupB").first()
                .unwrap(or: DBWrapperError.unknownGroup(id: "groupB")).thenThrowing { try $0.objects.query(on: conn).count() }.flatMap { $0 }
        }
        guard let c1 = try? query1.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(c1, 1)

        let query = container.withPooledConnection(to: .mysql) { conn in
            ObjectTable.query(on: conn).filter(\.className == "Person").filter(\.objectId == "oB1").first().map { $0 == nil ? false : true }
        }
        guard let c = try? query.wait() else {
            XCTFail()
            return
        }
        XCTAssertTrue(c)

        let groupQuery = container.withPooledConnection(to: .mysql) {
            GroupTable.query(on: $0).filter(\.name == "groupB").first().unwrap(or: DBWrapperError.unknownGroup(id: "groupB"))
        }

        guard let group = try? groupQuery.wait() else {
            XCTFail()
            return
        }

        let signaturesQuery = container.withPooledConnection(to: .mysql) { conn in
            try group.signatures.query(on: conn).all()
        }

        guard let signatures = try? signaturesQuery.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(signatures.count, 1)
        guard let sig1 = signatures.first else {
            XCTFail()
            return
        }
        XCTAssertEqual(sig1.version, 1)
        let logsQuery = container.withPooledConnection(to: .mysql) { try sig1.logs.query(on: $0).all() }

        guard let logs = try? logsQuery.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(logs.count, 1)
        guard let log1 = logs.first else {
            XCTFail()
            return
        }
        XCTAssertEqual(log1.updateType, .creation)

        let attributesQuery = container.withPooledConnection(to: .mysql) { try log1.attributes.query(on: $0).all() }
        guard let attributes1 = try? attributesQuery.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(attributes1.count, 2)
        let attrs1 = Set(attributes1.map { "\(($0.key, String(data: $0.value!, encoding: .utf8)!))" })
        XCTAssertEqual(attrs1, Set(["(\"age\", \"63\")", "(\"name\", \"B\")"]))

        let uns = mysql.unsubscribe(MySQLUser(id: "B"), from: MySQLGroup(id: "groupB", version: 0), by: userB).asResult
        XCTAssertNil(uns.asFailure)

        guard let b = mysql.groups(for: userB).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(b.count, 1, "\(b)")

        let query2 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupB").first().map { $0 == nil ? false : true }
        }
        guard let c2 = try? query2.wait() else {
            XCTFail()
            return
        }
        XCTAssertFalse(c2)
        let query3 = container.withPooledConnection(to: .mysql) { conn in
            ObjectTable.query(on: conn).filter(\.className == "Person").filter(\.objectId == "oB1").first().map { $0 == nil ? false : true }
        }
        guard let c3 = try? query3.wait() else {
            XCTFail()
            return
        }
        XCTAssertFalse(c3)

        let signaturesQuery1 = container.withPooledConnection(to: .mysql) { conn in
            SignatureTable.find(sig1.id!, on: conn).unwrap(or: DBWrapperError.unknownGroup(id: ""))
        }

        let signatures1 = try? signaturesQuery1.wait()
        XCTAssertNil(signatures1)

        let logsQuery1 = container.withPooledConnection(to: .mysql) { LogTable.find(log1.id!, on: $0).unwrap(or: DBWrapperError.unknownGroup(id: "")) }

        let log2 = try? logsQuery1.wait()
        XCTAssertNil(log2)
        let at1 = attributes1[0]
        let at2 = attributes1[1]
        let at1Query = container.withPooledConnection(to: .mysql) { AttributeTable.find(at1.id!, on: $0).unwrap(or: DBWrapperError.unknownGroup(id: "")) }
        let at2Query = container.withPooledConnection(to: .mysql) { AttributeTable.find(at2.id!, on: $0).unwrap(or: DBWrapperError.unknownGroup(id: "")) }

        let at11 = try? at1Query.wait()

        XCTAssertNil(at11)

        let at21 = try? at2Query.wait()
        XCTAssertNil(at21)
    }

    func testRemoveGroup() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A").get
        guard let userA = _auser else {
            XCTFail()
            return
        }

        let _buser = mysql.user(by: "B").get
        guard let userB = _buser else {
            XCTFail()
            return
        }

        guard let a = mysql.groups(for: userA).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(a.count, 2, "\(a)")
        let exp1 = [MySQLGroup(id: "groupA", version: 2), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(a), Set(exp1))


        guard let b = mysql.groups(for: userB).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(b.count, 2, "\(b)")
        let exp2 = [MySQLGroup(id: "groupB", version: 1), MySQLGroup(id: "groupAB", version: 3)]
        XCTAssertEqual(Set(b), Set(exp2))

        let query1 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupAB").first()
                .unwrap(or: DBWrapperError.default).thenThrowing { try $0.objects.query(on: conn).count() }.flatMap { $0 }
        }
        guard let c1 = try? query1.wait() else {
            XCTFail()
            return
        }
        XCTAssertEqual(c1, 3)

        let query = container.withPooledConnection(to: .mysql) { conn in
            ObjectTable.query(on: conn).filter(\.className == "Person").filter(\.objectId == "oAB1").first().map { $0 == nil ? false : true }
        }
        guard let c = try? query.wait() else {
            XCTFail()
            return
        }
        XCTAssertTrue(c)

        let query4 = container.withPooledConnection(to: .mysql) { conn in
            ObjectTable.query(on: conn).filter(\.className == "Person").filter(\.objectId == "oAB2").first().map { $0 == nil ? false : true }
        }
        guard let c4 = try? query4.wait() else {
            XCTFail()
            return
        }
        XCTAssertTrue(c4)

        XCTAssertTrue(mysql.remove(group: MySQLGroup(id: "groupAB", version: 0), by: userA).getBool)

        guard let d = mysql.groups(for: userA).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(d.count, 1, "\(d)")
        guard let e = mysql.groups(for: userB).get as? [MySQLGroup] else {
            XCTFail()
            return
        }
        XCTAssertEqual(e.count, 1, "\(e)")

        let query2 = container.withPooledConnection(to: .mysql) { conn in
            GroupTable.query(on: conn).filter(\.name == "groupAB").first().map { $0 == nil ? false : true }
        }
        guard let c2 = try? query2.wait() else {
            XCTFail()
            return
        }
        XCTAssertFalse(c2)

        let query3 = container.withPooledConnection(to: .mysql) { conn in
            ObjectTable.query(on: conn).filter(\.className == "Person").filter(\.objectId == "oAB1").first().map { $0 == nil ? false : true }
        }
        guard let c3 = try? query3.wait() else {
            XCTFail()
            return
        }
        XCTAssertFalse(c3)

        let query5 = container.withPooledConnection(to: .mysql) { conn in
            ObjectTable.query(on: conn).filter(\.className == "Person").filter(\.objectId == "oAB2").first().map { $0 == nil ? false : true }
        }
        guard let c5 = try? query5.wait() else {
            XCTFail()
            return
        }
        XCTAssertFalse(c5)
    }

    func testDiffAFrom0() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A").get
        guard let userA = _auser else {
            XCTFail()
            return
        }
        let _diff = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupA", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let c1 = MySQLCreationDescription(groupID: "groupA", object: MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 2512, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("A")), MySQLAttributeUpdate(attribute: "age", value: .uint(15))]))
        let u1 = MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 2511, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(20))])
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupA", version: 2),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oA1")], role: .owner)],
                                transactions: [
                                    MySQLTransaction(deletions: [], creations: [c1], modifications: [u1])
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffAFrom1() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        let _diff = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupA", version: 1)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let u1 = MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 2511, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(20))])
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupA", version: 2),
                                 newObjects: [], role: .owner)],
                                transactions: [
                                    MySQLTransaction(deletions: [], creations: [], modifications: [u1])
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffAFrom2() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        let _diff = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupA", version: 2)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupA", version: 2),
                                 newObjects: [], role: .owner)],
                                transactions: [])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffBFrom0() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }

        let _diff = mysql.diffs(for: userB, in: [MySQLGroup(id: "groupB", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let c1 = MySQLCreationDescription(groupID: "groupB", object: MySQLSubTransaction(className: "Person", objectId: "oB1", timestamp: refDate - 100, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("B")), MySQLAttributeUpdate(attribute: "age", value: .uint(63))]))

        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupB", version: 1),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oB1")], role: .owner)],
                                transactions: [
                                    MySQLTransaction(deletions: [], creations: [c1], modifications: [])
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffBFrom1() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }
        let _diff = mysql.diffs(for: userB, in: [MySQLGroup(id: "groupB", version: 1)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupB", version: 1),
                                 newObjects: [], role: .owner)],
                                transactions: [])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffABFrom0() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }

        let _diff = mysql.diffs(for: userB, in: [MySQLGroup(id: "groupAB", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let c2 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 3500, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Tom")), MySQLAttributeUpdate(attribute: "age", value: .uint(23))]))
        let c3 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 1824, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Lucy")), MySQLAttributeUpdate(attribute: "age", value: .uint(22))]))
        let u2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(25))])
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oAB2"), MySQLObjectDescription(className: "Person", objectId: "oAB3")], role: .read)],
                                transactions: [
                                    MySQLTransaction(deletions: [], creations: [c2, c3], modifications: [u2])
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffABFrom1() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }

        let _diff = mysql.diffs(for: userB, in: [MySQLGroup(id: "groupAB", version: 1)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let c3 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 1824, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Lucy")), MySQLAttributeUpdate(attribute: "age", value: .uint(22))]))
        let u2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(25))])
        let d1 = MySQLSubTransaction(className: "Person", objectId: "oAB1", timestamp: refDate - 1015, updates: [])
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oAB3")], role: .read)],
                                transactions: [
                                    MySQLTransaction(deletions: [d1], creations: [c3], modifications: [u2])
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffABFrom2() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }
        let _diff = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupAB", version: 2)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let u2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(25))])
        let d1 = MySQLSubTransaction(className: "Person", objectId: "oAB1", timestamp: refDate - 1015, updates: [])
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [], role: .owner)],
                                transactions: [
                                    MySQLTransaction(deletions: [d1], creations: [], modifications: [u2])
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffABFrom3() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }
        let _diff = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupAB", version: 3)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [], role: .owner)],
                                transactions: [])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffCFrom0() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _cuser = mysql.user(by: "C")
        guard let userC = _cuser.get else {
            XCTFail()
            return
        }
        let _diff = mysql.diffs(for: userC, in: [MySQLGroup(id: "groupC", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupC", version: 0),
                                 newObjects: [], role: .owner)],
                                transactions: [])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffAFrom0ByC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _cuser = mysql.user(by: "C")
        guard let userC = _cuser.get else {
            XCTFail()
            return
        }
        let _diff = mysql.diffs(for: userC, in: [MySQLGroup(id: "groupA", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let expDiff = MySQLDiff(groups: [],
                                transactions: [])
        XCTAssertEqual(diff, expDiff)
    }

    func testDiffABAndBFrom2And0ByB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }

        let _diff = mysql.diffs(for: userB, in: [MySQLGroup(id: "groupAB", version: 2), MySQLGroup(id: "groupB", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }

        let u2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(25))])
        let d1 = MySQLSubTransaction(className: "Person", objectId: "oAB1", timestamp: refDate - 1015, updates: [])
        let c1 = MySQLCreationDescription(groupID: "groupB", object: MySQLSubTransaction(className: "Person", objectId: "oB1", timestamp: refDate - 100, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("B")), MySQLAttributeUpdate(attribute: "age", value: .uint(63))]))
        
        let expDiff = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3), newObjects: [], role: .read),
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupB", version: 1), newObjects: [MySQLObjectDescription(className: "Person", objectId: "oB1")], role: .owner)
            ],
                                transactions: [
                                    MySQLTransaction(deletions: [d1], creations: [], modifications: [u2]),
                                    MySQLTransaction(deletions: [], creations: [c1], modifications: []),
        ])
        XCTAssertEqual(diff, expDiff)
    }

    func testUpdateOneWayGroupCByCCreateModifInOne() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "C")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        let c1 = MySQLCreationDescription(groupID: "groupC", object: MySQLSubTransaction(className: "Person", objectId: "oC2", timestamp: refDate - 511, updates: []))
        let u4 = MySQLSubTransaction(className: "Person", objectId: "oC2", timestamp: refDate - 501, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("C2")), MySQLAttributeUpdate(attribute: "age", value: .uint(10))])
        let transaction = MySQLTransaction(deletions: [], creations: [c1], modifications: [u4])

        let _diff = mysql.update(groups: [MySQLGroup(id: "groupC", version: 0)], with: transaction, by: userA)
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let expDiff1 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupC", version: 1),
                                 newObjects: [],
                                 role: .owner)],
                                transactions: [])
        let expDiff2 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupC", version: 1),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oC2")],
                                 role: .owner)],
                                transactions: [
                                    MySQLTransaction(deletions: [], creations: [c1], modifications: [u4])
        ])

        XCTAssertEqual(diff, expDiff1)


        let _diff2 = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupC", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff2 = _diff2.get as? MySQLDiff else {
            XCTFail()
            return
        }

        XCTAssertEqual(diff2, expDiff2)
    }

    func testUpdateEmptyByC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "C")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }


        let transaction = MySQLTransaction(deletions: [], creations: [], modifications: [])

        let _diff = mysql.update(groups: [MySQLGroup(id: "groupC", version: 0)], with: transaction, by: userA)
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }

        let expDiff1 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupC", version: 0),
                                 newObjects: [],
                                 role: .owner)],
                                transactions: [])
        let expDiff2 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupC", version: 0),
                                 newObjects: [],
                                 role: .owner)],
                                transactions: [])

        XCTAssertEqual(diff, expDiff1)


        let _diff2 = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupC", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff2 = _diff2.get as? MySQLDiff else {
            XCTFail()
            return
        }

        XCTAssertEqual(diff2, expDiff2)
    }

    func testUpdateOneWayGroupAGroupABByA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        let c1 = MySQLCreationDescription(groupID: "groupA", object: MySQLSubTransaction(className: "Person", objectId: "oA2", timestamp: refDate - 511, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("A2")), MySQLAttributeUpdate(attribute: "age", value: .uint(10))]))
        let u1 = MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 415, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(300))])
        let u2 = MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 414, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(30))])
        let u3 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 14, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(30)), MySQLAttributeUpdate(attribute: "name", value: .string("Marko"))])
        let u4 = MySQLSubTransaction(className: "Person", objectId: "oA2", timestamp: refDate - 511, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(31))])
        let d1 = MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 241, updates: [])
        let transaction = MySQLTransaction(deletions: [d1], creations: [c1], modifications: [u1, u2, u3, u4])

        let _diff = mysql.update(groups: [MySQLGroup(id: "groupAB", version: 3), MySQLGroup(id: "groupA", version: 2)], with: transaction, by: userA)
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }
        let c11 = MySQLCreationDescription(groupID: "groupA", object: MySQLSubTransaction(className: "Person", objectId: "oA2", timestamp: refDate - 511, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("A2"))]))
        let ca1 = MySQLCreationDescription(groupID: "groupA", object: MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 2512, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("A"))]))
        let cab2 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 3500, updates: []))

        let uab2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 14, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Marko")), MySQLAttributeUpdate(attribute: "age", value: .uint(30))])
        let u5 = MySQLSubTransaction(className: "Person", objectId: "oA1", timestamp: refDate - 414, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(30))])

        let expDiff1 = MySQLDiff(groups: [
        MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupA", version: 3),
                             newObjects: [],
                             role: .owner),
        MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 4),
                             newObjects: [], role: .owner),],
                                transactions: [])
        let expDiff2 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupA", version: 3),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oA2"), MySQLObjectDescription(className: "Person", objectId: "oA1")],
                                 role: .owner),
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 4),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oAB2")], role: .owner)],
                                transactions: [
                                MySQLTransaction(deletions: [], creations: [c11, ca1], modifications: [u4, u5]),
                                MySQLTransaction(deletions: [], creations: [cab2], modifications: [uab2]),
        ])

        XCTAssertEqual(diff, expDiff1)


        let _diff2 = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupAB", version: 0), MySQLGroup(id: "groupA", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff2 = _diff2.get as? MySQLDiff else {
            XCTFail()
            return
        }

        XCTAssertEqual(diff2, expDiff2)
    }

    func testUpdateTwoWayGroupBGroupABByB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _buser = mysql.user(by: "B")
        guard let userB = _buser.get else {
            XCTFail()
            return
        }

        let c1 = MySQLCreationDescription(groupID: "groupB", object: MySQLSubTransaction(className: "Person", objectId: "oB2", timestamp: refDate - 511, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("B2")), MySQLAttributeUpdate(attribute: "age", value: .uint(10))]))
        let u1 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 415, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(300))])
        let c2 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB4", timestamp: refDate - 414, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("AB4")), MySQLAttributeUpdate(attribute: "age", value: .uint(15))]))
        let d1 = MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 241, updates: [])
        let transaction = MySQLTransaction(deletions: [d1], creations: [c1, c2], modifications: [u1, ])

        let _diff = mysql.update(groups: [MySQLGroup(id: "groupAB", version: 3), MySQLGroup(id: "groupB", version: 0)], with: transaction, by: userB)
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }

        let cb1 = MySQLCreationDescription(groupID: "groupB", object: MySQLSubTransaction(className: "Person", objectId: "oB1", timestamp: refDate - 100, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("B")), MySQLAttributeUpdate(attribute: "age", value: .uint(63))]))
        let cab2 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 3500, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Tom")), MySQLAttributeUpdate(attribute: "age", value: .uint(23))]))
        let cab3 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 1824, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Lucy")), MySQLAttributeUpdate(attribute: "age", value: .uint(22))]))
        let uab2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(25))])
        let rbc2 = MySQLSubTransaction(className: "Person", objectId: "oAB4", timestamp: Date.distantPast, updates: [])
        let rbab3 = MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 1824, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Lucy")), MySQLAttributeUpdate(attribute: "age", value: .uint(22))])
        let rbab2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Tom")), MySQLAttributeUpdate(attribute: "age", value: .uint(25))])

        let expDiff1 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupB", version: 2),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oB1")], role: .owner),
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [], role: .read)],
                                transactions: [MySQLTransaction(deletions: [rbc2], creations: [cb1], modifications: [rbab3, rbab2])])
        let expDiff2 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupB", version: 2),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oB1"), MySQLObjectDescription(className: "Person", objectId: "oB2")],
                                 role: .owner),
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oAB2"), MySQLObjectDescription(className: "Person", objectId: "oAB3")], role: .read)],
                                transactions: [
                                MySQLTransaction(deletions: [], creations: [cb1, c1], modifications: []),
                                MySQLTransaction(deletions: [], creations: [cab2, cab3], modifications: [uab2])
        ])

        XCTAssertEqual(diff, expDiff1)


        let _diff2 = mysql.diffs(for: userB, in: [MySQLGroup(id: "groupAB", version: 0), MySQLGroup(id: "groupB", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff2 = _diff2.get as? MySQLDiff else {
            XCTFail()
            return
        }

        XCTAssertEqual(diff2, expDiff2)
    }

    func testUpdateTwoWayGroupBByBConflictClientWins() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        let u1 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 415, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(300))])
        let transaction = MySQLTransaction(deletions: [], creations: [], modifications: [u1])

        let _diff = mysql.update(groups: [MySQLGroup(id: "groupAB", version: 2)], with: transaction, by: userA)
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }

        let cab2 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 3500, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Tom"))]))
        let cab3 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 1824, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Lucy")), MySQLAttributeUpdate(attribute: "age", value: .uint(22))]))

        let dab1 = MySQLSubTransaction(className: "Person", objectId: "oAB1", timestamp: refDate - 1015, updates: [])

        let expDiff1 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 4),
                                 newObjects: [], role: .owner)],
                                 transactions: [MySQLTransaction(deletions: [dab1], creations: [], modifications: [])])
        let expDiff2 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 4),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oAB2"), MySQLObjectDescription(className: "Person", objectId: "oAB3")], role: .owner)],
                                 transactions: [
                                    MySQLTransaction(deletions: [], creations: [cab2, cab3], modifications: [u1])
        ])

        XCTAssertEqual(diff, expDiff1)

        let _diff2 = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupAB", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff2 = _diff2.get as? MySQLDiff else {
            XCTFail()
            return
        }

        XCTAssertEqual(diff2, expDiff2)
    }


    func testUpdateTwoWayGroupBByBConflictServerWins() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let _auser = mysql.user(by: "A")
        guard let userA = _auser.get else {
            XCTFail()
            return
        }

        let u1 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 2000, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(300))])
        let transaction = MySQLTransaction(deletions: [], creations: [], modifications: [u1])

        let _diff = mysql.update(groups: [MySQLGroup(id: "groupAB", version: 2)], with: transaction, by: userA)
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff = _diff.get as? MySQLDiff else {
            XCTFail()
            return
        }

        let cab2 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 3500, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Tom")), MySQLAttributeUpdate(attribute: "age", value: .uint(23))]))
        let cab3 = MySQLCreationDescription(groupID: "groupAB", object: MySQLSubTransaction(className: "Person", objectId: "oAB3", timestamp: refDate - 1824, updates: [MySQLAttributeUpdate(attribute: "name", value: .string("Lucy")), MySQLAttributeUpdate(attribute: "age", value: .uint(22))]))

        let uab2 = MySQLSubTransaction(className: "Person", objectId: "oAB2", timestamp: refDate - 1200, updates: [MySQLAttributeUpdate(attribute: "age", value: .uint(25))])
        let dab1 = MySQLSubTransaction(className: "Person", objectId: "oAB1", timestamp: refDate - 1015, updates: [])

        let expDiff1 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [], role: .owner)],
                                 transactions: [MySQLTransaction(deletions: [dab1], creations: [], modifications: [uab2])])
        let expDiff2 = MySQLDiff(groups: [
            MySQLDiffGroupUpdate(group: MySQLGroup(id: "groupAB", version: 3),
                                 newObjects: [MySQLObjectDescription(className: "Person", objectId: "oAB2"), MySQLObjectDescription(className: "Person", objectId: "oAB3")], role: .owner)],
                                 transactions: [
                                    MySQLTransaction(deletions: [], creations: [cab2, cab3], modifications: [uab2])
        ])

        XCTAssertEqual(diff, expDiff1)


        let _diff2 = mysql.diffs(for: userA, in: [MySQLGroup(id: "groupAB", version: 0)])
        XCTAssertNil(_diff.asResult.asFailure)
        guard let diff2 = _diff2.get as? MySQLDiff else {
            XCTFail()
            return
        }

        XCTAssertEqual(diff2, expDiff2)
    }

    func testGetUsersForGroupA() {
        let g = MySQLGroup(id: "groupA", version: 0)
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let r = mysql.users(for: g).asResult
        XCTAssertNil(r.asFailure)
        guard let users = r.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(users, [UserRole(user: MySQLUser(id: "A"), role: .owner)])
    }

    func testGetUsersForGroupAB() {
        let g = MySQLGroup(id: "groupAB", version: 0)
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let r = mysql.users(for: g).asResult
        XCTAssertNil(r.asFailure)
        guard let users = r.asSuccess.map({ $0.sorted { $0.user.id < $1.user.id } }) else {
            XCTFail()
            return
        }
        XCTAssertEqual(users, [UserRole(user: MySQLUser(id: "A"), role: .owner), UserRole(user: MySQLUser(id: "B"), role: .read)])
    }

    func testGetUsersForGroupB() {
        let g = MySQLGroup(id: "groupB", version: 0)
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let r = mysql.users(for: g).asResult
        XCTAssertNil(r.asFailure)
        guard let users = r.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(users, [UserRole(user: MySQLUser(id: "B"), role: .owner)])
    }

    func testGetUsersForGroupC() {
        let g = MySQLGroup(id: "groupC", version: 0)
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let r = mysql.users(for: g).asResult
        XCTAssertNil(r.asFailure)
        guard let users = r.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(users, [UserRole(user: MySQLUser(id: "C"), role: .owner)])
    }

    func testAddDeviceToUserC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userC = MySQLUser(id: "C")

        let r = mysql.devices(for: userC).asResult
        XCTAssertNil(r.asFailure)

        guard let devices1 = r.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices1.map { $0.id }, [])
        XCTAssertTrue(mysql.add(device: MySQLDevice(id: "deviceC", description: "iOS"), for: userC).getBool)

        guard let devices2 = mysql.devices(for: userC).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices2.count, 1)
        XCTAssertEqual(devices2.map { $0.id }, ["deviceC"])
    }

    func testAddDeviceToUserA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userA = MySQLUser(id: "A")

        guard let devices1 = mysql.devices(for: userA).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices1.count, 1)
        XCTAssertEqual(devices1.map { $0.id }, ["deviceA1"])

        XCTAssertTrue(mysql.add(device: MySQLDevice(id: "deviceA2", description: "iOS"), for: userA).getBool)

        guard let devices2 = mysql.devices(for: userA).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices2.count, 2)
        XCTAssertEqual(Set(devices2.map { $0.id }), Set(["deviceA1", "deviceA2"]))
    }

    func testAddDeviceToUserB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userB = MySQLUser(id: "B")

        guard let devices1 = mysql.devices(for: userB).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices1.count, 2)
        XCTAssertEqual(Set(devices1.map { $0.id }), Set(["deviceB1", "deviceB2"]))

        XCTAssertTrue(mysql.add(device: MySQLDevice(id: "deviceB3", description: "iOS"), for: userB).getBool)

        guard let devices2 = mysql.devices(for: userB).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices2.count, 3)
        XCTAssertEqual(Set(devices2.map { $0.id }), Set(["deviceB1", "deviceB2", "deviceB3"]))
    }

    func testAddDeviceToUserD() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userD = MySQLUser(id: "D")

        let devices1 = mysql.devices(for: userD).get
        XCTAssertNil(devices1)

        XCTAssertFalse(mysql.add(device: MySQLDevice(id: "deviceB3", description: "iOS"), for: userD).getBool)

        let devices2 = mysql.devices(for: userD).get
        XCTAssertNil(devices2)
    }

    func testRemoveDeviceForUserA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userA = MySQLUser(id: "A")

        guard let devices1 = mysql.devices(for: userA).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices1.count, 1)
        XCTAssertEqual(devices1.map { $0.id }, ["deviceA1"])

        XCTAssertTrue(mysql.remove(deviceId: "deviceA1", for: userA).getBool)

        guard let devices2 = mysql.devices(for: userA).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices2.map { $0.id }, [])
        XCTAssertEqual(devices2.count, 0)
    }

    func testRemoveDeviceForUserB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userB = MySQLUser(id: "B")

        guard let devices1 = mysql.devices(for: userB).get else {
            XCTFail()
            return
        }

        XCTAssertEqual(devices1.count, 2)
        XCTAssertEqual(Set(devices1.map { $0.id }), Set(["deviceB1", "deviceB2"]))

        XCTAssertTrue(mysql.remove(deviceId: "deviceB1", for: userB).getBool)

        guard let devices2 = mysql.devices(for: userB).get else {
            XCTFail()
            return
        }

        XCTAssertEqual(devices2.count, 1)
        XCTAssertEqual(devices2.map { $0.id }, ["deviceB2"])

        XCTAssertTrue(mysql.remove(deviceId: "deviceB2", for: userB).getBool)

        guard let devices3 = mysql.devices(for: userB).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices3.map { $0.id }, [])
        XCTAssertEqual(devices3.count, 0)
    }

    func testRemoveDeviceForUserC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userC = MySQLUser(id: "C")

        guard let devices1 = mysql.devices(for: userC).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices1.map { $0.id }, [])
        XCTAssertTrue(mysql.remove(deviceId: "deviceC", for: userC).getBool)

        guard let devices2 = mysql.devices(for: userC).get else {
            XCTFail()
            return
        }
        XCTAssertEqual(devices2.map { $0.id }, [])
    }

    func testRemoveDeviceForUserD() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userD = MySQLUser(id: "D")

        let devices1 = mysql.devices(for: userD).get
        XCTAssertNil(devices1)

        XCTAssertFalse(mysql.remove(deviceId: "deviceD", for: userD).getBool)

        let devices2 = mysql.devices(for: userD).get
        XCTAssertNil(devices2)
    }

    func testGetRoleDInD() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let user = MySQLUser(id: "D")
        let group = MySQLGroup(id: "groupD", version: 0)
        let r = mysql.role(of: user, in: group).asResult
        XCTAssertNil(r.asSuccess)
        guard case let .unknownUser(id) = r.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id, "D")
    }

    func testGetRoleDInA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let user = MySQLUser(id: "D")
        let group = MySQLGroup(id: "groupA", version: 0)
        let r = mysql.role(of: user, in: group).asResult
        XCTAssertNil(r.asSuccess)
        guard case let .unknownUser(id) = r.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id, "D")
    }

    func testGetRoleAInA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let user = MySQLUser(id: "A")
        let group = MySQLGroup(id: "groupA", version: 0)
        let r = mysql.role(of: user, in: group).asResult
        XCTAssertNil(r.asFailure)
        XCTAssertEqual(r.asSuccess, .owner)
    }

    func testGetRoleAInB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let user = MySQLUser(id: "A")
        let group = MySQLGroup(id: "groupB", version: 0)
        let r = mysql.role(of: user, in: group).asResult
        XCTAssertNil(r.asSuccess)
        guard case let .userNotInGroup(u, g) = r.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(u.id, user.id)
        XCTAssertEqual(g.id, group.id)
    }

    func testGetRoleBInB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let user = MySQLUser(id: "B")
        let group = MySQLGroup(id: "groupB", version: 0)
        let r = mysql.role(of: user, in: group).asResult
        XCTAssertNil(r.asFailure)
        XCTAssertEqual(r.asSuccess, .owner)
    }

    func testGetRoleBInAB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let user = MySQLUser(id: "B")
        let group = MySQLGroup(id: "groupAB", version: 0)
        let r = mysql.role(of: user, in: group).asResult
        XCTAssertNil(r.asFailure)
        XCTAssertEqual(r.asSuccess, .read)
    }

    func testCreateNewUser() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let u1 = mysql.user(by: "D").asResult
        XCTAssertNil(u1.asSuccess)
        guard case let .unknownUser(id) = u1.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id, "D")
        let u2 = mysql.addUser(withId: "D").asResult
        XCTAssertNil(u2.asFailure)
        guard let u21 = u2.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(u21.id, "D")
        let u3 = mysql.user(by: "D").asResult
        XCTAssertNil(u3.asFailure)
        guard let u31 = u3.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(u31.id, "D")
    }

    func testCreateExistingUser() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)

        let u2 = mysql.user(by: "A").asResult
        XCTAssertNil(u2.asFailure)
        guard let u21 = u2.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertEqual(u21.id, "A")

        let u1 = mysql.addUser(withId: "A").asResult
        XCTAssertNil(u1.asSuccess)
        guard case let .userAlreadyExists(id) = u1.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id, "A")
    }

    func testRemoveUserD() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userD = MySQLUser(id: "D")

        let a = mysql.user(by: userD.id).asResult
        XCTAssertNil(a.asSuccess)
        guard case let .unknownUser(id) = a.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id, "D")

        let r = mysql.remove(user: userD).asResult
        XCTAssertNil(r.asFailure)

        let b = mysql.user(by: userD.id).asResult
        XCTAssertNil(b.asSuccess)
        guard case let .unknownUser(id1) = b.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id1, "D")
    }

    func testRemoveUserC() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userC = MySQLUser(id: "C")

        let g1 = mysql.groups(for: userC).asResult
        XCTAssertNil(g1.asFailure)
        XCTAssertEqual(g1.asSuccess?.count, 1)

        guard let g = g1.asSuccess?.first else {
            XCTFail()
            return
        }

        let a = mysql.user(by: userC.id).asResult
        XCTAssertNil(a.asFailure)
        XCTAssertEqual(a.asSuccess?.id, "C")

        let r = mysql.remove(user: userC).asResult
        XCTAssertNil(r.asFailure)

        let b = mysql.user(by: userC.id).asResult
        XCTAssertNil(b.asSuccess)
        guard case let .unknownUser(id1) = b.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id1, "C")

        let query1 = container.withPooledConnection(to: .mysql) { conn in GroupTable.query(on: conn).filter(\.name == g.id).first() }.asResult
        XCTAssertNil(query1.asFailure)
        guard let s = query1.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertNil(s)
    }


    func testRemoveUserB() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userB = MySQLUser(id: "B")

        let g1 = mysql.groups(for: userB).asResult
        XCTAssertNil(g1.asFailure)
        XCTAssertEqual(g1.asSuccess?.count, 2)
        XCTAssertTrue(g1.asSuccess?.map { $0.id }.contains("groupAB") ?? false)

        guard let g = g1.asSuccess?.filter({ $0.id == "groupB" }).first else {
            XCTFail()
            return
        }

        let a = mysql.user(by: userB.id).asResult
        XCTAssertNil(a.asFailure)
        XCTAssertEqual(a.asSuccess?.id, "B")

        let r = mysql.remove(user: userB).asResult
        XCTAssertNil(r.asFailure)

        let b = mysql.user(by: userB.id).asResult
        XCTAssertNil(b.asSuccess)
        guard case let .unknownUser(id1) = b.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id1, "B")

        let query1 = container.withPooledConnection(to: .mysql) { conn in GroupTable.query(on: conn).filter(\.name == g.id).first() }.asResult
        XCTAssertNil(query1.asFailure)
        guard let s = query1.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertNil(s)

        let query2 = container.withPooledConnection(to: .mysql) { conn in GroupTable.query(on: conn).filter(\.name == "groupAB").first() }.asResult
        XCTAssertNil(query2.asFailure)
        guard let s1 = query2.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertNotNil(s1)
    }

    func testRemoveUserA() {
        let container = getContainer()
        let mysql = getMeerkatMySQL(from: container)
        let userA = MySQLUser(id: "A")

        let g1 = mysql.groups(for: userA).asResult
        XCTAssertNil(g1.asFailure)
        XCTAssertEqual(g1.asSuccess?.count, 2)

        guard let gA = g1.asSuccess?.filter({ $0.id == "groupA" }).first else {
            XCTFail()
            return
        }

        guard let gAB = g1.asSuccess?.filter({ $0.id == "groupAB" }).first else {
            XCTFail()
            return
        }

        let a = mysql.user(by: userA.id).asResult
        XCTAssertNil(a.asFailure)
        XCTAssertEqual(a.asSuccess?.id, "A")

        let r = mysql.remove(user: userA).asResult
        XCTAssertNil(r.asFailure)

        let b = mysql.user(by: userA.id).asResult
        XCTAssertNil(b.asSuccess)
        guard case let .unknownUser(id1) = b.asFailure as? DBWrapperError else {
            XCTFail()
            return
        }
        XCTAssertEqual(id1, "A")

        let query1 = container.withPooledConnection(to: .mysql) { conn in GroupTable.query(on: conn).filter(\.name == gA.id).first() }.asResult
        XCTAssertNil(query1.asFailure)
        guard let s = query1.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertNil(s)

        let query2 = container.withPooledConnection(to: .mysql) { conn in GroupTable.query(on: conn).filter(\.name == gAB.id).first() }.asResult
        XCTAssertNil(query2.asFailure)
        guard let s1 = query2.asSuccess else {
            XCTFail()
            return
        }
        XCTAssertNil(s1)
    }

    static var allTests = [
        ("testUser", testUser),
        ("testGroupsA", testGroupsA),
        ("testGroupsB", testGroupsB),
        ("testGroupsC", testGroupsC),
        ("testGroupA", testGroupA),
        ("testgroupB", testGroupB),
        ("testGroupC", testGroupC),
        ("testGroupAB", testGroupAB),
        ("testAddGroup", testAddGroup),
        ("testSubscribeToGroup", testSubscribeToGroup),
        ("testUnsubscribeWrongUserFromGroup", testUnsubscribeWrongUserFromGroup),
        ("testUnsubscribeFromGroup", testUnsubscribeFromGroup),
        ("testUnsubscribeFromGroupAndRemove", testUnsubscribeFromGroupAndRemove),
        ("testRemoveGroup", testRemoveGroup),
        ("testDiffAFrom0", testDiffAFrom0),
        ("testDiffAFrom1", testDiffAFrom1),
        ("testDiffAFrom2", testDiffAFrom2),
        ("testDiffBFrom0", testDiffBFrom0),
        ("testDiffBFrom1", testDiffBFrom1),
        ("testDiffABFrom0", testDiffABFrom0),
        ("testDiffABFrom1", testDiffABFrom1),
        ("testDiffABFrom2", testDiffABFrom2),
        ("testDiffABFrom3", testDiffABFrom3),
        ("testDiffAFrom0ByC", testDiffAFrom0ByC),
        ("testUpdateOneWayGroupCByCCreateModifInOne", testUpdateOneWayGroupCByCCreateModifInOne),
        ("testUpdateOneWayGroupAGroupABByA", testUpdateOneWayGroupAGroupABByA),
        ("testUpdateTwoWayGroupBGroupABByB", testUpdateTwoWayGroupBGroupABByB),
        ("testUpdateTwoWayGroupBByBConflictClientWins", testUpdateTwoWayGroupBByBConflictClientWins),
        ("testUpdateTwoWayGroupBByBConflictServerWins", testUpdateTwoWayGroupBByBConflictServerWins),
        ("testGetUsersForGroupA", testGetUsersForGroupA),
        ("testGetUsersForGroupB", testGetUsersForGroupB),
        ("testGetUsersForGroupAB", testGetUsersForGroupAB),
        ("testGetUsersForGroupC", testGetUsersForGroupC),
        ("testAddDeviceToUserA", testAddDeviceToUserA),
        ("testAddDeviceToUserB", testAddDeviceToUserB),
        ("testAddDeviceToUserC", testAddDeviceToUserC),
        ("testAddDeviceToUserD", testAddDeviceToUserD),
        ("testRemoveDeviceForUserA", testRemoveDeviceForUserA),
        ("testRemoveDeviceForUserB", testRemoveDeviceForUserB),
        ("testRemoveDeviceForUserC", testRemoveDeviceForUserC),
        ("testRemoveDeviceForUserD", testRemoveDeviceForUserD),
        ("testGetRoleDInD", testGetRoleDInD),
        ("testGetRoleDInA", testGetRoleDInA),
        ("testGetRoleAInA", testGetRoleAInA),
        ("testGetRoleAInB", testGetRoleAInB),
        ("testGetRoleBInB", testGetRoleBInB),
        ("testGetRoleBInAB", testGetRoleBInAB),
        ("testCreateNewUser", testCreateNewUser),
        ("testCreateExistingUser", testCreateExistingUser),
        ("testRemoveUserA", testRemoveUserA),
        ("testRemoveUserB", testRemoveUserB),
        ("testRemoveUserC", testRemoveUserC),
        ("testRemoveUserD", testRemoveUserD),
    ]
}

private func createUser(id: String, container: Container) -> (UserTable, GroupTable) {
    let r = try! container.withPooledConnection(to: .mysql, closure: { con in
        UserTable(username: id).save(on: con)
            .and( { GroupTable(name: "group\(id)").save(on: con) }())
            .then { u, g in
                RoleTable(user: u.id!, group: g.id!, role: .owner)
                    .save(on: con)
                    .map { _ in
                        return (u, g)
                }
        }
    }).wait()
    return r
}

private func createObject(className: String, objectId: String, in group: GroupTable, container: Container) -> ObjectTable {
    let r = container.withPooledConnection(to: .mysql) { con in
        ObjectTable(className: className, objectId: objectId, groupId: group.id!).save(on: con)
    }
    return try! r.wait()
}


private func createAttribute(key: String, value: String, in log: LogTable, on con: MySQLConnection) -> EventLoopFuture<AttributeTable> {
    return AttributeTable(key: key, value: value.data(using: .utf8)!, logId: log.id!).save(on: con)
}

private func createPersonLog(name: String, age: Int, timestamp: Date, object: ObjectTable, signature: SignatureTable, on con: MySQLConnection) -> EventLoopFuture<LogTable> {

    let createLog = LogTable(timestamp: timestamp, updateType: .creation, objectId: object.id!, signatureId: signature.id!).save(on: con)
    return createLog.flatMap { log in
        createAttribute(key: "name", value: name, in: log, on: con).and(createAttribute(key: "age", value: age.description, in: log, on: con)).transform(to: log)
    }
}

private func modifyPersonLog(name: String? = nil, age: Int? = nil, timestamp: Date, object: ObjectTable, signature: SignatureTable, on con: MySQLConnection) -> EventLoopFuture<LogTable> {

    let createLog = LogTable(timestamp: timestamp, updateType: .modification, objectId: object.id!, signatureId: signature.id!).save(on: con)
    return createLog.flatMap { log in

        func feature(key: String, value: String?) -> EventLoopFuture<Void> {
            guard let v = value else {
                return con.future()
            }
            return createAttribute(key: key, value: v, in: log, on: con).transform(to: ())
        }

        let u1 = feature(key: "name", value: name)
        let u2 = feature(key: "age", value: age?.description)
        return u1.and(u2).transform(to: log)
    }
}

private func deletePersonLog(timestamp: Date, object: ObjectTable, signature: SignatureTable, on con: MySQLConnection) -> EventLoopFuture<LogTable> {
    let createLog = LogTable(timestamp: timestamp, updateType: .deletion, objectId: object.id!, signatureId: signature.id!).save(on: con)
    return createLog
}

extension Future {
    var get: T? {
        return try? wait()
    }

    var asResult: Result<T, Error> {
        .init { () -> T in
            try wait()
        }
    }
}

extension Future where T == Void {
    var getBool: Bool {
        return try! transform(to: true).mapIfError { _ in false }.wait()
    }
}

extension Result {
    var asFailure: Failure? {
        switch self {
        case .success:
            return nil
        case .failure(let err):
            return err
        }
    }
    var asSuccess: Success? {
        switch self {
        case .success(let s):
            return s
        case .failure:
            return nil
        }
    }

    var isSuccess: Bool {
        return asFailure == nil
    }

    var isFailure: Bool {
        return !isSuccess
    }
}

extension Error {
    static var `default`: Error { DBWrapperError.unknownGroup(id: "UNKNOWN ERROR") }
}

extension UserRole: Equatable {
    public static func == (lhs: UserRole, rhs: UserRole) -> Bool {
        guard let lu = lhs.user as? MySQLUser else {
            return false
        }
        guard let ru = rhs.user as? MySQLUser else {
            return false
        }
        return lu == ru && lhs.role == rhs.role
    }
}
