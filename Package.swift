// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatMySQL",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatMySQL",
            targets: ["MeerkatMySQL"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatservercore", from: "1.0.0"),
        .package(url: "https://gitlab.com/MeerkatSync/meerkatschemedescriptor", from: "1.0.0"),
        .package(url: "https://github.com/vapor/fluent-mysql.git", from: "3.0.0"),
    ],
    targets: [
        .target(
            name: "MeerkatMySQL",
            dependencies: ["MeerkatServerCore", "FluentMySQL", "MeerkatSchemeDescriptor"]),
        .testTarget(
            name: "MeerkatMySQLTests",
            dependencies: ["MeerkatMySQL"]),
    ]
)
