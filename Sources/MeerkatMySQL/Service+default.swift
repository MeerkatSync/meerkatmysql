//
//  Service+default.swift
//  MeerkatMySQL
//
//  Created by Filip Klembara on 20/01/2020.
//

import Crypto

public extension Services {
    static let meerkatMySQLDefault: Services = {
        var services = Services()

        // bcrypt
        services.register { container -> BCryptDigest in
            return .init()
        }
        // keyed cache
        services.register(MemoryKeyedCache(), as: KeyedCache.self)

        // console
        services.register(Console.self) { container -> Terminal in
            return Terminal()
        }

        // logging
        services.register(Logger.self) { container -> ConsoleLogger in
            return try ConsoleLogger(console: container.make())
        }
        services.register(Logger.self) { container -> PrintLogger in
            return PrintLogger()
        }

        // blocking IO pool is thread safe
        let sharedThreadPool = BlockingIOThreadPool(numberOfThreads: 2)
        sharedThreadPool.start()
        services.register(sharedThreadPool)
        
        try! services.register(MeerkatMySQLProvider())

        return services
    }()
}

extension BCryptDigest: Service { }
extension Terminal: Service { }
extension ConsoleLogger: Service { }
extension PrintLogger: Service { }
extension BlockingIOThreadPool: Service {}
extension MeerkatSchemeDescriptor: Service { }
