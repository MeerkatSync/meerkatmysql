//
//  GroupTable.swift
//  Async
//
//  Created by Filip Klembara on 20/01/2020.
//

struct GroupTable {
    var id: Int?
    let name: String

    static var name = "sync_group"
    static var entity = "sync_groups"
}

extension GroupTable {
    var users: Siblings<GroupTable, UserTable, RoleTable> {
        return siblings()
    }

    var signatures: Children<GroupTable, SignatureTable> {
        return children(\.groupId)
    }

    var objects: Children<GroupTable, ObjectTable> {
        return children(\.groupId)
    }
}

extension GroupTable: MySQLModel { }
extension GroupTable: MySQLMigration { }

extension GroupTable: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}
