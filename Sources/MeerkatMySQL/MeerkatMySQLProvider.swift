//
//  MeerkatMySQLProvider.swift
//  Async
//
//  Created by Filip Klembara on 06/02/2020.
//

public struct MeerkatMySQLProvider {
    public init() { }

    public func setUpMigration(_ migration: inout MigrationConfig) {
        migration.add(model: DeviceTable.self, database: .mysql)
        migration.add(model: RoleTable.self, database: .mysql)
        migration.add(model: UserTable.self, database: .mysql)
        migration.add(model: GroupTable.self, database: .mysql)
        migration.add(model: ObjectTable.self, database: .mysql)
        migration.add(model: SignatureTable.self, database: .mysql)
        migration.add(model: LogTable.self, database: .mysql)
        migration.add(model: AttributeTable.self, database: .mysql)
    }
}

extension MeerkatMySQLProvider: Provider {
    public func register(_ services: inout Services) throws {
        try services.register(FluentProvider())
        try services.register(MySQLProvider())

        services.register(MeerkatMySQL.self)
        
        var migrations = MigrationConfig()
        setUpMigration(&migrations)
        services.register(migrations)
    }

    public func didBoot(_ container: Container) throws -> EventLoopFuture<Void> {
        .done(on: container)
    }
}
