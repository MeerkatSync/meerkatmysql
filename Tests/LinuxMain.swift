import XCTest

import MeerkatMySQLTests

var tests = [XCTestCaseEntry]()
tests += MeerkatMySQLTests.allTests()
XCTMain(tests)
